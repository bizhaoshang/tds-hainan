<?php

namespace App\Repos;

use App\Models\DiancanShop;

class DiancanShopRepo 
{
    public static function getList($where=[])
    {    	
    	return DiancanShop::where($where)
    						->orderBy('weight','asc')
    						->get();
    }

    public static function find($where=[])
    {
    	return DiancanShop::where($where)->first();
    }
}
