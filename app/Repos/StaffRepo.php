<?php

namespace App\Repos;

use App\Models\Staff;
use App\Models\DiancanOrder;
use App\Models\DiancanCart;
use App\Models\DiancanProduct;

class StaffRepo 
{
    public static function get($where=[])
    {
    	return Staff::where($where)->select('id','name')->get();
    }

    public static function find($where)
    {
    	return Staff::where($where)->first();
    }

    public static function create($data)
    {
    	$data['token'] = str_random(16);
    	$rs = Staff::create($data);
    	return $rs;
    }

    public static function update($where,$data)
    {
        return Staff::where($where)->update($data);
    }


    public  static function checkMoney($staff_id,$product_id)
    {

        $rs['status'] = true;
        // 查询今天已下单额度
        $today_money = DiancanOrder::where(['staff_id'=>$staff_id])->whereDate('created_at',date('Y-m-d'))->sum('total_price');

        // 本次商品的价格
        $product = DiancanProduct::where(['id'=>$product_id])
                                 ->first();
        // 购物车总量                         
        $cart_total = DiancanCart::where(['staff_id'=>$staff_id])->sum('subtotal');
                                 
        $user_money = Staff::where('id',$staff_id)->value('money');   
        //dd($today_money+$product->price+$cart_total,$user_money);                         
        if($cart_total>$user_money) {
            $rs['status'] = false;
            $rs['msg'] = '余额不足- -！当前余额：'.$user_money;
            return $rs;
        } 

        return $rs;   
    }
}
