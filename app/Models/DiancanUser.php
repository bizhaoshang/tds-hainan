<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class DiancanUser extends Model
{
    protected $guarded = ['id'];


     protected $appends = ['order_num','month_money','sub_order_num','sub_month_money','sub2_order_num','sub2_month_money'];



    public function orders()
    {
    	return $this->hasMany(DiancanOrder::class,'user_id');
    }

    	
    public function getOrderNumAttribute()
    {
    	$num = DiancanOrder::where(['user_id'=>$this->id])
    				->whereMonth('created_at', Carbon::now()->month)
    				->count();
    	return $num;
    }

    public function getMonthMoneyAttribute()
    {
        $num = DiancanOrder::where(['user_id'=>$this->id])
                    ->whereYear('created_at',Carbon::now()->year)
                    ->whereMonth('created_at', Carbon::now()->month)
                    ->sum('total_price');
        return $num;
    }

    public function getSubOrderNumAttribute()
    {
        $year = date('Y');
        $num = DiancanOrder::where(['user_id'=>$this->id])
                    ->whereYear('created_at',$year)
                    ->whereMonth('created_at', Carbon::now()->subMonth())
                    ->count();
        return $num;
    }

    public function getSubMonthMoneyAttribute()
    {
        $year = date('Y');
        $num = DiancanOrder::where(['user_id'=>$this->id])
                    ->whereYear('created_at',$year)
                    ->whereMonth('created_at', Carbon::now()->subMonth())
                    ->sum('total_price');
        return $num;
    }



    public function getSub2OrderNumAttribute()
    {
        $year = date('Y');
        $num = DiancanOrder::where(['user_id'=>$this->id])
                    ->whereYear('created_at',$year)
                    ->whereMonth('created_at', Carbon::now()->subMonths(2))
                    ->count();
        return $num;
    }

    public function getSub2MonthMoneyAttribute()
    {
        $year = date('Y');
        $num = DiancanOrder::where(['user_id'=>$this->id])
                    ->whereYear('created_at',$year)
                    ->whereMonth('created_at', Carbon::now()->subMonths(2))
                    ->sum('total_price');
        return $num;
    }

    
}
