<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kfquestion extends Model
{
    //
    public $table = 'kf_question';
    protected $guarded = ['id'];
    public function answer()
    {
          return $this->hasMany('App\Models\Kfanswer', 'question_id', 'id');
    }
}
