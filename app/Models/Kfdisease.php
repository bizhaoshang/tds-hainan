<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kfdisease extends Model
{
    protected $table = 'kf_diseases';
    protected $primaryKey = 'id';
    public    $timestamps = false;
    protected $guarded = ['id'];

   

    
    public function symptom_disease()
    {
        return $this->belongsToMany(Kfsymptom::class,'kf_symptom_diseases','diseases_id','symptom_id')->withPivot('probability');
    }


    public function department()
    {
        return $this->belongsToMany(Kfdepartment::class,'kf_diseases_department','diseases_id','department_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Kftags::class,'kf_tag_diseases','did','tid');
    }
}
