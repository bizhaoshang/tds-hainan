<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KfContentDepartment extends Model
{
    protected $table = 'kf_content_departments';
    protected $primaryKey = 'id';
   
    protected $guarded = ['id'];

    public function content()
    {
    	return $this->belongsTo(KfCate::class);
    }

    public function department()
    {
    	return $this->belongsTo(Kfdepartment::class);
    }

  

}
