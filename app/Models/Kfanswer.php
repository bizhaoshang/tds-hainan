<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kfanswer extends Model
{
    //
    public $table = 'kf_answer';
    protected $guarded = ['id'];

    public function question()
    {
        return $this->belongsTo('App\Models\Kfquestion', 'question_id', 'id');
    }
}
