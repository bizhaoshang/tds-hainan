<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Staff extends Model
{
    public $table = 'staffs';
    protected $guarded = ['id'];

     protected $appends = ['order_num','headimg'];

    public function orders()
    {
    	return $this->hasMany(DiancanOrder::class);
    }

    	
    public function getOrderNumAttribute()
    {
    	$num = DiancanOrder::where(['staff_id'=>$this->id])
    				->whereMonth('created_at', Carbon::now()->month)
    				->count();
    	return $num;
    }

    public function getHeadimgAttribute()
    {
        $pos = strpos($this->avatar, 'http');

        // 注意这里使用的是 ===。简单的 == 不能像我们期待的那样工作，
        // 因为 'a' 是第 0 位置上的（第一个）字符。
        if ($pos === false) {
            return config('app.url').$this->avatar;
        } else {
            return $this->avatar;
        }
       
    }
    
}
