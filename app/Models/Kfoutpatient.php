<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kfoutpatient extends Model
{
    //
    public $table = 'kf_outpatient';
    protected $guarded = ['id'];

}
