<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kftags extends Model
{
    protected $table = 'kf_tags';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $guarded = [];

    protected $fillable = ['name', 'status','image'];

    public function tagSymptom()
    {
        return $this->hasMany(KfTagSymptom::class,'tag_id','id');
    }

}
