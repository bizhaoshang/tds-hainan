<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KfContent extends Model
{
    protected $table = 'kf_content';
    protected $primaryKey = 'id';
   
    protected $guarded = ['id'];

    public function cate()
    {
    	return $this->belongsTo(KfCate::class);
    }

  

}
