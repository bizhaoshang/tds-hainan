<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaffAddress extends Model
{
    protected $guarded = ['id'];
}
