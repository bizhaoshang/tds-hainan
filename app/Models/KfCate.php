<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KfCate extends Model
{
    protected $table = 'kf_cates';
    protected $primaryKey = 'id';
   
    protected $guarded = ['id'];

  
    public function contents()
    {
    	return $this->hasMany(KfContent::class,'cate_id','id');
    }
}
