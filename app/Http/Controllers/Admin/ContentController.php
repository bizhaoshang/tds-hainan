<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageUploadHandlers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\StandardTagFactory;
use phpDocumentor\Reflection\Types\Integer;
use App\Models\KfCate;
use App\Models\KfContent;
use App\Models\Kfdepartment;
use App\Models\KfContentDepartment;
use DB;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $where = [];
        $list = KfContent::where($where)->orderBy('id','desc')->paginate(20);
        
        return view('admin.content.index',compact('list'));
    }

    public function create(Request $request)
    {
        $cates   = KfCate::get();  
        $departments = Kfdepartment::get();
        return view('admin.content.add',compact('cates','departments'));
    }

    public function store(Request $request)
    {
        $data = $request->except('_token','department_ids');
        if ($request->hasFile('pic')) {           
            $filename = date('YmdHis').'.'.$request->pic->extension();
            $request->pic->storeAs('public/pic',$filename);            
            $data['pic']  = '/storage/pic/'.$filename; 
        } 

        $kc = KfContent::create($data);
        if($request->department_ids) {
            foreach ($request->department_ids as $k => $v) {
                $info['department_id'] = $v;
                $info['content_id']    = $kc->id;
                KfContentDepartment::create($info);
            }            
        }
        $rs['status'] = true;
        $rs['msg']    = '操作成功';
        return redirect('zadmin/content')->with('rs',$rs);
    }


    public function edit($id)
    {
        $content = KfContent::find($id);
        $cates   = KfCate::get();  
        $departments    = Kfdepartment::get();
        $department_ids = KfContentDepartment::where(['content_id'=>$id])
                                        ->pluck('department_id')
                                        ->toArray();
        return view('admin.content.edit',compact('content','cates','departments','department_ids'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token','_method','department_ids');
        if ($request->hasFile('pic')) {           
            $filename = date('YmdHis').'.'.$request->pic->extension();
            $request->pic->storeAs('public/pic',$filename);            
            $data['pic']  = '/storage/pic/'.$filename; 
        } 
        $data = KfContent::where('id',$id)->update($data);


        if($request->department_ids) {
            foreach ($request->department_ids as $k => $v) {
                $info['department_id'] = $v;
                $info['content_id']    = $id;
                KfContentDepartment::updateOrcreate($info);
            }            
        }
        $rs['status'] = true;
        $rs['msg']    = '操作成功';
        return redirect('zadmin/content')->with('rs',$rs);
    }


    


    

    public function destroy ($id)
    {   
      
        KfContent::destroy($id);
        return back();
    }
}

