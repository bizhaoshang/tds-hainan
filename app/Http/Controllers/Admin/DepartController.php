<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Depart;
use App\Models\DiancanUser;

class DepartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Depart::where('pid',0)->paginate(20);
        $parents = Depart::where(['pid'=>0])->get();
        return view('admin.depart.index',compact('list','parents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.depart.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
       
        $rs   = Depart::create($data);
        if($rs) {
            return redirect('zadmin/depart');
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Depart::find($id);
        $parents = Depart::where(['pid'=>0])->get();
        return view('admin.depart.edit',compact('data','parents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token','_method');
       
        $depart   = Depart::find($id);
        $oldDepart = $depart->name;
        $depart->name   = $request->name;
        $depart->pid    = $request->pid;
        $depart->remark = $request->remark;
        $depart->save();

        $rs = DiancanUser::where(['department'=>$oldDepart])->update(['department'=>$data['name']]);
        if($rs) {
            return redirect('zadmin/depart');
        }
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rs = Depart::destroy($id);
        if ($rs) {
            return redirect('zadmin/depart');
        }
        return back();
    }
}
