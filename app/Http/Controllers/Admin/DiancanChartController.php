<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DiancanOrder;


class DiancanChartController extends Controller
{
    public function index()
    {   // 89179.00 ,5039
        $month = 10;
        $total_price = DiancanOrder::whereMonth('created_at',$month)->sum('total_price');
        $total_ordernum = DiancanOrder::whereMonth('created_at',$month)->count();
        dd($total_price,$total_ordernum);
    	return view('admin.diancanchart.index');
    }

}    
