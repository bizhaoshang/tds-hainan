<?php

namespace App\Http\Controllers\Dz;

use App\Models\Kfquestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $where = [];
        $query = Kfquestion::query();
        if($request->name) {
            $query->where('name','like','%'.$request->name.'%');
            $where['name'] = $request->name;
        }
        //分页暂时修改,后期想办法解决分页问题
        $list =  $query->paginate(20);

        return view('daozhen.question.index',compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('daozhen.question.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->except('_token');

        if ($request->hasFile('img')) {
            $filename = date('YmdHis').'.'.$request->img->extension();
            $request->img->storeAs('public/pic',$filename);
            $data['img']  = '/storage/pic/'.$filename;
        }
        $doctor = Kfquestion::create($data);
        return redirect('daozhen/question');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kfquestion $question)
    {
        return view('daozhen.question.edit',compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kfquestion $kfquestion)
    {
        //
        $data = $request->except('_token');

        if ($request->hasFile('img')) {
            $filename = date('YmdHis').'.'.$request->img->extension();
            $request->img->storeAs('public/pic',$filename);
            $data['img']  = '/storage/pic/'.$filename;
        }
        $msg = $kfquestion->update($data);

        if($msg) {
            return redirect()->route('question.index')->with(['success'=>'修改成功']);
        } else {
            return back()->withErrors(['修改失败']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request,Kfquestion $kfquestion)
    {
        $question_data = $kfquestion->find($request->id);
        $msg = $question_data->delete();
        if($msg) {
            $data = ['code'=>200,'msg'=>'删除成功'];
        } else {
            $data = ['code'=>400,'msg'=>'删除失败'];
        }
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }
}
