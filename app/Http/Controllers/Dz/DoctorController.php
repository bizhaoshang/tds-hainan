<?php

namespace App\Http\Controllers\Dz;

use App\Filters\DoctorFilters;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Kfdoctor;
use App\Models\Kfdepartment;
use App\Models\KfdoctorDepartment;
use App\Helpers\ImageUploadHandlers;


class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $where = [];
       $query = Kfdoctor::query();
       if($request->name) {
            $query->where('name','like','%'.$request->name.'%');
            $where['name'] = $request->name;
       }

       if($request->department) {
            $doctor_ids = KfdoctorDepartment::where('department_id',$request->department)->pluck('doctor_id')->toArray();
            $query->whereIn('id',$doctor_ids);
            $where['department'] = $request->department;
       }
       
        $doctor =  $query->paginate(20);
        $total = Kfdoctor::count();
        $department = Kfdepartment::get();
        return view('daozhen.doctor.index',compact('doctor','department','total','where'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $department = Kfdepartment::get();
        return view('daozhen.doctor.add',compact('department'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token', 'department');
        if ($request->hasFile('image')) {           
            $filename = date('YmdHis').'.'.$request->image->extension();
            $request->image->storeAs('public/pic',$filename);            
            $data['image']  = '/storage/pic/'.$filename; 
        } 
        $doctor = Kfdoctor::create($data);
        foreach ($request->department as $key => $value) {
            $dd['department_id'] = $value;
            $dd['doctor_id'] = $doctor->id;
            KfdoctorDepartment::create($dd);
        }

        return redirect('daozhen/doctor');
        

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Kfdoctor $doctor)
    {

        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kfdoctor $doctor)
    {
        //
        $department = Kfdepartment::get();
        $department_id = $doctor->department->pluck('id')->all();
        return view('daozhen.doctor.edit',compact('doctor','department','department_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kfdoctor $doctor)
    {
        $data = $request->except('_token','_method','department');
        if ($request->hasFile('image')) {           
            $filename = date('YmdHis').'.'.$request->image->extension();
            $request->image->storeAs('public/pic',$filename);            
            $data['image']  = '/storage/pic/'.$filename; 
        } 
        $doctor->update($data);
        $msg = $doctor->department()->sync($request->department);
        if($msg) {
            return redirect()->route('doctor.index')->with(['success'=>'修改成功']);
        } else {
            return back()->withErrors(['失败']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, Kfdoctor $doctor)
    {
//        dd($request->id);
        $doctor_data = $doctor->find($request->id);
        $msg = $doctor_data->delete();
        $doctor_data->department()->detach();
//        $request->id
        if($msg) {
            $data = ['code'=>200,'msg'=>'删除成功'];
        } else {
            $data = ['code'=>400,'msg'=>'删除失败'];
        }
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);

    }
}
