<?php

namespace App\Http\Controllers\Dz;
use App\Models\Kfdepartment;
use App\Models\Kfdisease;
use App\Models\Kfsymptom;
use App\Models\Kftags;
use App\Models\KfSymptomDisease;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Admin\Traits\MessageTraits;
use URL;
use Session;

class DiseaseController extends Controller
{
    use MessageTraits;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $where = [];
        $flag = Kfdisease::query();
        if($request->keyword){
            $where['keyword'] = $request->keyword;
            $flag->where('name','like','%'.$request->keyword.'%');
        }
        if($request->department) {
            $dids = \DB::table('kf_diseases_department')->where('department_id',$request->department)->get()->pluck('diseases_id')->all();
            $flag->orWhereIn('id',$dids);
        }
        $list       = $flag->orderBy('id','desc')->paginate(15);
        $department = Kfdepartment::select('name','id')->get();
        $total = Kfdisease::count();
        $page = $request->page? :1;
        $num  = 20;
        $start = $page>1?(($page-1)*$num+1):1;
        return view('daozhen.disease.index',compact('list','where','department','total','start'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Kfdepartment $department)
    {
        $department =  $department->get();
        $tag = Kftags::get();
        return view('daozhen.disease.add',compact('department','tag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rs['status'] = 'danger';
        $rs['msg'] = '操作失败';
        $data = $request->except('_token','tags','department');
        $where['name'] = trim($data['name']);
        $flag = Kfdisease::updateOrCreate($where,$data);
        $did  = $flag->id;
        $flag->department()->attach($request->department);
        $flag->tags()->attach($request->tags);
        $fenci = app('pinyin')->abbr($request->name);
        \DB::table('kf_diseases_fenci')->insert(['pinyin'=>$fenci,'did'=>$did]);

        if ($flag){
            $rs['status'] = 'success';
            $rs['msg'] = '操作成功';
            return redirect('daozhen/disease')->with('rs',$rs);
        }
        $rs['mag'] = $flag['msg'];
        return back()->withInput()->with('rs',$rs);
    }

   
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Kfdepartment $department)
    {
        $data = Kfdisease::find($id);
        $department =  $department->get();
//        $department_id =  \DB::table('kf_diseases_department')->where('diseases_id',$id)->get()->pluck('department_id')->all();
//        $department_id = empty($department_id)  ? 0 : $department_id;
        $tag = Kftags::get();
        $department_id = $data->department->pluck('id')->all();
        $tags_id = $data->tags->pluck('id')->all();

        $myreferrer = Session::get('myreferrer', URL::previous());
        return view('daozhen.disease.edit',compact('tag','data','department','department_id','tags_id','department_id','myreferrer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token','_method','department','tags','myreferrer');
        $flag =  Kfdisease::find($id);
        $flag->update($data);
        $flag->department()->sync($request->department);
        $flag->tags()->sync($request->tags);
        $pinyin = app('pinyin')->abbr($request->name);
        \DB::table('kf_diseases_fenci')->where('did',$id)->update(['pinyin'=>$pinyin]);

        if ($flag){
            return redirect($request->myreferrer);
        }
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Kfdisease::find($id)->symptom_disease()->detach();
        \DB::table('kf_diseases_department')->where('diseases_id',$id)->delete();
        $flag = Kfdisease::find($id);
        $flag->department()->detach();
        $flag->tags()->detach();
        $flag->destroy($id);
        \DB::table('kf_diseases_fenci')->where('did',$id)->delete();

        if ($flag){
            return redirect('daozhen/disease');
        }

        return back();
    }

    public function bathDel(Request $request)
    {

        $rs['status'] = false;
        $ids = $request->ids;
        $flag = Kfdisease::where('id',$ids)->delete();
        if($flag) {
            $rs['status'] = true;
            return response()->json($rs);
        }
        return response()->json($rs);
    }
    
    public function dorela($id)
    {
        $data = Kfdisease::find($id);
       
        $result = Kfsymptom::all();

        $checkeds = DB::table('kf_symptom_diseases')
                    ->where('diseases_id',$id)
                    ->select(['symptom_id','probability'])
                    ->get()
                    ->toArray();
        $symptom_ids = array_column($checkeds, 'symptom_id');
        $probabilitys = array_column($checkeds, 'probability','symptom_id');
        $tag = Kftags::select('id','name')->get();
        return view('daozhen.disease.symptom',compact('data','result','symptom_ids','probabilitys','tag'));
    }

    public function insertdata(Request $request,$id)
    {
        $disease_id = $id;
        $symptom_id  = $request->symptom_id;
        $arr = [];
        $member = [];
        $probability = $request->probability;
        $delete = KfSymptomDisease::where('diseases_id',$id)->delete();
        foreach($symptom_id as $k=>$v){
            $arr['diseases_id'] = $disease_id;
            $arr['symptom_id']  = $v;
            $arr['probability'] = $probability[$k];
            $data   = KfSymptomDisease::create($arr);
        }
        
       
        $rs['status'] = 'danger';
        $rs['msg'] = '操作失败';
        if ($data){
            $rs['status'] = 'success';
            $rs['msg'] = '操作成功';
            return redirect('daozhen/disease')->with('rs',$rs);
        }
        $rs['mag'] = $flag['msg'];
        return back()->withInput()->with('rs',$rs);
    }

    public function search(Request $request,Kfdisease $disease)
    {
        $msg = $disease->where('name','like','%'.$request->name.'%')->first();
        if($msg) {
            $msg2 =  $msg->symptom_disease->pluck('id');
            if($msg2) {
                $data = ['code'=>200,'msg'=>'成功~','data'=>$msg2,'data2'=>$msg];
            } else {
                $data = ['code'=>400,'msg'=>'没有该疾病~'];
            }
        } else {
            $data = ['code'=>400,'msg'=>'没有该疾病1~'];
        }

        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    public function tag_search(Request $request)
    {
        $tag_id = DB::table('kf_tag_symptom')->where('tag_id',$request->id)->get()->pluck('symptom_id');
        if($tag_id){
            $data = ['code'=>200,'msg'=>'成功','data'=>$tag_id];
        }else {
            $data = ['code'=>400,'msg'=>'失败'];
        }
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

}
