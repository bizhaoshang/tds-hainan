<?php

namespace App\Http\Controllers\Dz;

use App\Helpers\ImageUploadHandlers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\StandardTagFactory;
use phpDocumentor\Reflection\Types\Integer;
use App\Models\KfCate;
use App\Models\KfContent;
use App\Models\Kfdepartment;
use App\Models\KfContentDepartment;
use DB;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $where = [];
        if($request->cate_id) {
            $where['cate_id'] = $request->cate_id;
        }
        $list = KfContent::where($where)->orderBy('id','desc')->paginate(20);
        $cates   = KfCate::get(); 
        return view('daozhen.content.index',compact('list','cates'));
    }


    public function create(Request $request)
    {
        $cates   = KfCate::get();  
        $departments = Kfdepartment::get();
        return view('daozhen.content.add',compact('cates','departments'));
    }

    public function store(Request $request)
    {
        $data = $request->except('_token','department_ids');
        if ($request->hasFile('pic')) {           
            $filename = date('YmdHis').'.'.$request->pic->extension();
            $request->pic->storeAs('public/pic',$filename);            
            $data['pic']  = '/storage/pic/'.$filename; 
        } 

        if ($request->hasFile('thumb')) {           
            $filename = date('YmdHis').str_random(3).'.'.$request->thumb->extension();
            $request->thumb->storeAs('public/pic',$filename);            
            $data['thumb']  = '/storage/pic/'.$filename; 
        } 

        $kc = KfContent::create($data);
        if($request->department_ids) {
            foreach ($request->department_ids as $k => $v) {
                $info['department_id'] = $v;
                $info['content_id']    = $kc->id;
                KfContentDepartment::create($info);
            }            
        }
        $rs['status'] = true;
        $rs['msg']    = '操作成功';
        return redirect('daozhen/content')->with('rs',$rs);
    }


    public function edit($id)
    {
        $content = KfContent::find($id);
        $cates   = KfCate::get();  
        $departments    = Kfdepartment::get();
        $department_ids = KfContentDepartment::where(['content_id'=>$id])
                                        ->pluck('department_id')
                                        ->toArray();
        return view('daozhen.content.edit',compact('content','cates','departments','department_ids'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token','_method','department_ids');
        if ($request->hasFile('pic')) {           
            $filename = date('YmdHis').'.'.$request->pic->extension();
            $request->pic->storeAs('public/pic',$filename);            
            $data['pic']  = '/storage/pic/'.$filename; 
        } 

        if ($request->hasFile('thumb')) {           
            $filename = date('YmdHis').str_random(3).'.'.$request->thumb->extension();
            $request->thumb->storeAs('public/pic',$filename);            
            $data['thumb']  = '/storage/pic/'.$filename; 
        } 

        $data = KfContent::where('id',$id)->update($data);

        KfContentDepartment::where(['content_id'=>$id])->delete();
        if($request->department_ids) {
            foreach ($request->department_ids as $k => $v) {
                $info['department_id'] = $v;
                $info['content_id']    = $id;
                KfContentDepartment::create($info);
            }            
        }
        $rs['status'] = true;
        $rs['msg']    = '操作成功';
        return redirect('daozhen/content')->with('rs',$rs);
    }


    


    

    public function destroy($id)
    {   
      
        KfContent::destroy($id);
        return back();
    }

}

