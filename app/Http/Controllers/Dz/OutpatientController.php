<?php

namespace App\Http\Controllers\Dz;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Kfdoctor;
use App\Models\Kfoutpatient;

class OutpatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $where = [];
        $query = Kfoutpatient::query();
        if($request->name) {
            $query->where('name','like','%'.$request->name.'%');
            $where['name'] = $request->name;
        }
        //分页暂时修改,后期想办法解决分页问题
        $list =  $query->paginate(30)->toArray();
        foreach($list['data'] as $k=>&$v){
            $v['doctor'] = explode(',',$v['doctor']);
            foreach ($v['doctor'] as $kk=>$vv){
                $doctor = Kfdoctor::where('id',$vv)->pluck('name')->toArray();

                $v['doctor_name'][$kk] = $doctor[0];
            }
        }
        unset($v);
        $list = $list['data'];
        return view('daozhen.outpatient.index',compact('list'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $doctor = Kfdoctor::get();

        return view('daozhen.outpatient.add',compact('doctor'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $data['doctor'] = join(",",$data['doctor']);

        if ($request->hasFile('image')) {
            $filename = date('YmdHis').'.'.$request->image->extension();
            $request->image->storeAs('public/pic',$filename);
            $data['image']  = '/storage/pic/'.$filename;
        }

        if ($request->hasFile('img')) {
            $filename = date('YmdHis').'.'.$request->img->extension();
            $request->img->storeAs('public/pic',$filename);
            $data['img']  = '/storage/pic/'.$filename;
        }
        $doctor = Kfoutpatient::create($data);
        return redirect('daozhen/outpatient');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kfoutpatient $outpatient)
    {
        //
        $doctor = Kfdoctor::get();
        $doctor_id = explode(',',$outpatient->doctor);
        return view('daozhen.outpatient.edit',compact('doctor','outpatient','doctor_id'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kfoutpatient $outpatient)
    {
        //
        $data = $request->except('_token','_method');

        $data['doctor'] = join(",",$data['doctor']);

        if ($request->hasFile('thumb')) {
            $filename = date('YmdHis').'.'.$request->thumb->extension();
            $request->thumb->storeAs('public/pic',$filename);
            $data['thumb']  = '/storage/pic/'.$filename;
        }
        if ($request->hasFile('img')) {
            $filename = date('YmdHis').'.'.$request->img->extension();
            $request->img->storeAs('public/pic',$filename);
            $data['img']  = '/storage/pic/'.$filename;
        }
        $msg = $outpatient->update($data);

        if($msg) {
            return redirect()->route('outpatient.index')->with(['success'=>'修改成功']);
        } else {
            return back()->withErrors(['修改失败']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request, Kfoutpatient $outpatient)
    {

        $outpatient_data = $outpatient->find($request->id);
        $msg = $outpatient_data->delete();
//        $request->id
        if($msg) {
            $data = ['code'=>200,'msg'=>'删除成功'];
        } else {
            $data = ['code'=>400,'msg'=>'删除失败'];
        }
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);

    }
}
