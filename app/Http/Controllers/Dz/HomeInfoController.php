<?php

namespace App\Http\Controllers\Dz;

use App\Helpers\ImageUploadHandlers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Models\DaozhenHomeInfo;
use DB;

class HomeInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $where = [];
        if($request->keyword) {
           
        }
        $list = DaozhenHomeInfo::where($where)->orderBy('id','desc')->paginate(20);
        
        return view('daozhen.home-info.index',compact('list'));
    }


    public function create(Request $request)
    {
       
       
        return view('daozhen.home-info.add');
    }

    public function store(Request $request)
    {
        $data = $request->except('_token');
        
        

        $kc = DaozhenHomeInfo::create($data);
       
        $rs['status'] = 'success';
        $rs['msg']    = '操作成功';
        return redirect('daozhen/home-info')->with('rs',$rs);
    }


    public function edit($id)
    {
        $data = DaozhenHomeInfo::find($id);
       
        return view('daozhen.home-info.edit',compact('data'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token','_method');
      
        DaozhenHomeInfo::where('id',$id)->update($data);

        $rs['status'] = 'success';
        $rs['msg']    = '操作成功';
        return redirect('daozhen/home-info')->with('rs',$rs);
    }


    


    

    public function destroy($id)
    {   
      
        DaozhenHomeInfo::destroy($id);
        return back();
    }

}

