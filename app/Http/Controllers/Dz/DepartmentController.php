<?php

namespace App\Http\Controllers\Dz;
use App\Models\Kfdisease;
use App\Models\Kftags;
use App\Models\Kfdepartment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Admin\Traits\MessageTraits;
use App\Filters\DepartmentFilters;


class DepartmentController extends Controller
{
    public function index(DepartmentFilters $filters,Kfdepartment $department)
    {
        $dep =  $department->Filter($filters)->orderBy('weight','asc')->paginate(10);
        return view('daozhen.department.index',compact('dep'));
    }


    public function create()
    {
        $tag = Kftags::get();
        return view('daozhen.department.create',compact('tag'));
    }

    public function store(Request $request)
    {
        $data = $request->except('_token','tags');
        Kfdepartment::create($data);
        Kftags::create($request->tags);
        return back();
    }

    public function edit(Kfdepartment $department)
    {
        $tag = Kftags::get();
        $tags_id = $department->tags->pluck('id')->all();
        return view('daozhen.department.edit',compact('department','tag','tags_id'));
    }


    public function update(Request $request,Kfdepartment $department)
    {
        $department->update($request->except('_token','method','tags'));
        $department->tags()->sync($request->tags);
        return $this->redirect_msg($department,route('department.index'),'修改');

    }

    public function delete(Request $request,Kfdepartment $department)
    {
        \DB::table('diseases_department')->where('department_id',$request->id)->delete();
        $department->find($request->id)->delete();

        return $this->json_msg($department,'修改');


    }
}
