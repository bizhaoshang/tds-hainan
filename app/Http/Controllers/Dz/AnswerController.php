<?php

namespace App\Http\Controllers\Dz;

use App\Models\Kfanswer;
use App\Models\Kfquestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $where = [];
        $query = Kfanswer::query();
        if($request->name) {
            $query->where('name','like','%'.$request->name.'%');
            $where['name'] = $request->name;
        }
        $list = $query->paginate(20);
        return view('daozhen.answer.index',compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $question = Kfquestion::get();
        return view('daozhen.answer.add',compact('question'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->except('_token');
        
        $doctor = Kfoutpatient::create($data);
        return redirect('daozhen/outpatient');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kfanswer $answer)
    {
        //
        $question = Kfquestion::get();
        return view('daozhen.answer.edit',compact('question','answer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kfanswer $answer)
    {
        //
        $data = $request->except('_token','method');
        $msg = $answer->update($data);

        if($msg) {
            return redirect()->route('answer.index')->with(['success'=>'修改成功']);
        } else {
            return back()->withErrors(['修改失败']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request, Kfanswer $answer)
    {
        $answer_id = $amswer->find($request->id);
        $msg = $outpatient_data->delete();

        if($msg) {
            $data = ['code'=>200,'msg'=>'删除成功'];
        } else {
            $data = ['code'=>400,'msg'=>'删除失败'];
        }
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }
}
