<?php

namespace App\Http\Controllers\Wx\Diancan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repos\DiancanShopRepo;

class ShopController extends Controller
{
    public function getShops(Request $request)
    {
    	$rs['status']    = true;
    	$where['status'] = 1;
    	$rs['data']      = DiancanShopRepo::getList($where);
    	return response()->json($rs);
    }

    public function info(Request $request)
    {
        $rs['status'] = true;
        $where['id']  = (int)$request->shop_id;
        $data = DiancanShopRepo::find($where);
        $rs['data'] = $data;
        return response()->json($rs);
    }
}
