<?php

namespace App\Http\Controllers\Wx\Diancan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DiancanNotice;

class HomeController extends Controller
{
    public function notice(Request $request)
    {
        $rs['status'] = true;
        $id = (int)$request->id;
        $data = DiancanNotice::find($id);
        $rs['data'] = $data;
        return response()->json($rs);
    }
}
