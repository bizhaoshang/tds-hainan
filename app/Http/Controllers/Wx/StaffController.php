<?php
namespace App\Http\Controllers\Wx;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ApiRequest;
use App\Repos\StaffRepo;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function info(ApiRequest $request)
    {
        $rs['status']    = true;        
        $rs['data']      = $request->user;       
        return response()->json($rs);

    }

    public function update(ApiRequest $request)
    {
        $rs['status'] = true;
        $where['id']  = $request->staff_id;
        $data = $request->except(['staff_id']);
        $rs['data'] = StaffRepo::update($where,$data);        
        return response()->json($rs);
    }


    
}
