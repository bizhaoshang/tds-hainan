<?php

namespace App\Http\Controllers\Wx;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StaffAddress;
use App\Http\Requests\ApiRequest;

class StaffAddressController extends Controller
{
    public function index(ApiRequest $request)
    {
    	$rs['status'] = true;
    	$where['staff_id'] = $request->staff_id; 
    	$list = StaffAddress::where($where)
                            ->orderBy('is_default','desc')
                            ->paginate(10);
    	$rs['data'] = $list;      
        return response()->json($rs);
    }

    public function info(ApiRequest $request)
    {
    	$rs['status']      = true;        
        $where['staff_id'] = $request->staff_id; 
        $where['id'] = $request->id;
        $data = StaffAddress::where($where)->first(); 
        $rs['data'] = $data;      
        return response()->json($rs);
    }

    public function store(ApiRequest $request)
    {
    	$rs['status']      = true;         
        $data = $request->pure();
        if($data['is_default']==1) {
        	$where['staff_id'] = $request->staff_id;      
        	StaffAddress::where($where)->update(['is_default'=>0]);
        }
        $flag = StaffAddress::create($data); 
        $rs['data'] = $flag;      
        return response()->json($rs);
    }

    public function update(ApiRequest $request)
    {
    	$rs['status']      = true;       
        $where['id'] = $request->id;
        $data = $request->pure();
        if($data['is_default']==1) {
            $where['staff_id'] = $request->staff_id;      
            StaffAddress::where($where)->update(['is_default'=>0]);
        }
        $flag = StaffAddress::where($where)->update($data); 
        $rs['data'] = $flag;      
        return response()->json($rs);
    }

    public function delete(ApiRequest $request)
    {
    	$rs['status']      = true;        
        $where['staff_id'] = $request->staff_id; 
        $where['id'] = $request->id;
        $data = StaffAddress::where($where)->delete(); 
        $rs['data'] = $data;      
        return response()->json($rs);
    }

    public function setdefault(ApiRequest $request)
    {
    	$rs['status']      = true;       
        $where['staff_id'] = $request->staff_id;      
        StaffAddress::where($where)->update(['is_default'=>0]);
        $where['id'] = $request->id;
        $flag = StaffAddress::where($where)->update(['is_default'=>1]);
        if(!$flag) {
        	$rs['status'] = false;
        	$rs['msg'] = '操作失败';
        	return response()->json($rs);

        }
        $rs['data'] = $flag;      
        return response()->json($rs);
    }
}
