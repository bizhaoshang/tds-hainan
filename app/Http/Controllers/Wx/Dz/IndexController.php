<?php

namespace App\Http\Controllers\Wx\Dz;

use App\Models\Kfanswer;
use App\Models\kfBody;
use App\Models\Kfdepartment;
use App\Models\Kfdisease;
use App\Models\Kfquestion;
use App\Models\Kftags;
use App\Models\Kfsymptom;
use App\Models\Kfdoctor;
use App\Models\KfContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Kfoutpatient;
class IndexController extends Controller
{
    public function tags(Request $request)
    {
        $rs['status'] = true;
        $tags = \DB::table('kf_tags')->where('status',1)->get();
        $rs['data'] = $tags;
        return response()->json($rs);
    }


    public function symptom(Request $request,Kftags $tag)
    {
        $rs['status'] = false;
        $tag = $tag->find($request->id);
        if($tag && object_get($tag,'tagSymptom')) {
            $rs['status'] = true;
            $symptom_ids = $tag->tagSymptom->pluck('symptom_id');
            $tag_msg = Kfsymptom::whereIn('id',$symptom_ids)
                        ->get()
                        ->toArray();
            $arr = array_chunk($tag_msg,16);
            $rs['data']  = $arr;
        }
        return response()->json($rs);
    }

    public function search(Request $request)
    {
        $rs['status'] = true;
        $symptom = \DB::table('kf_symptom')->where('name','like','%'. $request->keyword.'%')->take(5)->get()->pluck('name','id');
        $diseases = \DB::table('kf_diseases')->where('name','like','%'. $request->keyword.'%')->take(5)->get()->pluck('name','id');
        $data['symptom'] = $symptom;
        $data['diseases'] = $diseases;
        $rs['data'] = $data;
        return response()->json($rs);

    }


    public function bodySearch(Request $request)
    {
        $rs['status'] = true;
        if($request->type == 'son'){
            $data = \DB::table('kf_bodys')->where('sex',$request->gender)->orWhere('pid',$request->id)->get();
        } else {
            $data = \DB::table('kf_bodys')->where('sex',$request->gender)->where('id',$request->id)->orWhere('pid',$request->id)->get();
        }
        $rs['data'] = $data;
        return response()->json($rs);
    }

    public function bodyTab(Request $request,kfBody $body)
    {
        $rs['status'] = false;
        $son = $body->with('symptom')->find($request->id);

        if ($son && object_get($son,'symptom')){
        $rs['status'] = true;
            $sons = $son ->symptom->pluck('name','id');
            $rs['data'] = $sons;
        }

        return response()->json($rs);
    }

    public function symptomSearch(Request $request)
    {
        $rs['status'] = true;
        $data = [];
        
     
        $symp_name = explode(',',$request->symptom_word);
        $s_ids = Kfsymptom::whereIn('name',$symp_name)->pluck('id');

        $disease_id = DB::table('kf_symptom_diseases')
                    ->whereIn('symptom_id',$s_ids)          
                    ->pluck('diseases_id');
                    
        $symptom_ids = DB::table('kf_symptom_diseases')
                                ->whereIn('diseases_id',$disease_id)
                                ->whereNotIn('symptom_id',$s_ids)
                                ->groupBy('symptom_id')
                                ->pluck('symptom_id');
        
        $arr = Kfsymptom::whereIn('id',$symptom_ids)->select('id','name')->get()->toArray();
        $arr_length = count($arr);

        $arr_length>6?($num = $arr_length/2):($num=1);
        $rs['data'] = array_chunk($arr,$num);
        return response()->json($rs);
    }

    public function symptomFenxi(Request $request)
    {
        $rs['status'] = true;        
       
        $symp_name   = explode(',',$request->symptom_word);
        
        $symptom_ids = Kfsymptom::whereIn('name',$symp_name)
                                ->pluck('id')
                                ->toArray();       
        
        if(!$symptom_ids) {
            $rs['status'] = false;
            return response()->json($rs);
        } 

        $diseases_ids = DB::table('kf_symptom_diseases')
                    ->whereIn('symptom_id',$symptom_ids)
                    ->orderBy('probability','desc')                   
                    ->pluck('diseases_id')
                    ->toArray();
        if(!$diseases_ids) {
            $rs['status'] = false;
            return response()->json($rs);
        } 


        $data = Kfdisease::with('department')->whereIn('id',$diseases_ids)->take(5)->get();      
        $rs['data'] = $data;
        return response()->json($rs);

    }


    public function diseaseRetrieve(Request $request,Kfdisease $disease)
    {
        $rs['status'] = true;
        $data = [];
        $id = \DB::table('kf_diseases')->where('name',$request->diseasename)->first()->id;
        $data['xgzds'] = $disease->find($id);
        $data['jzkses'] =   $data['xgzds']->department->pluck('name');
        $data['symptoms'] =  $data['xgzds']->symptom_disease->pluck('name');
        $rs['data'] = $data;
        return response()->json($rs);
    }

    //医生搜索API
    public function searchDoctor(Request $request)
    {
        $rs['status'] = true;
        $doctor = \DB::table('kf_doctor')->where('name','like','%'. $request->keyword.'%')->get();
        $rs['data'] = $doctor;
        return response()->json($rs);
    }
 



    /**
     * 名医点击api
     * @param Request $request
     */
    public function doctor(Request $request)
    {
        $rs['status'] = true;

        $data = Kfdoctor::find($request->id);
        
        $rs['data'] = $data;
        return response()->json($rs);
    }


    public function department(Request $request)
    {
        $rs['status'] = true;
        $list = Kfdepartment::where('status',1)                
                ->orderBy('weight','asc')
                ->get()
                ->toArray();
        foreach ($list as $k => $v) {
            $c = DB::table('kf_content_departments as cd')
                ->join('kf_content as c','c.id','=','cd.content_id')
                ->select(['c.id','c.title','c.pic','c.content','c.created_at'])        
                ->where('cd.department_id',$v['id'])
                ->where('c.status',1)
                ->orderBy('created_at','desc')
                ->take(3)
                ->get();
            $list[$k]['articles'] = $c;    
        }        
        $rs['data'] = $list;
        return response()->json($rs);
    }

    public function departmentContent(Request $request)
    {
        $department_id = $request->department_id;
        $rs['status'] = true;
        $list = DB::table('kf_content_departments as cd')
                ->join('kf_content as c','c.id','=','cd.content_id')
                ->select(['c.id','c.title','c.pic','c.content','c.created_at'])        
                ->where('cd.department_id',$department_id)
                ->where('c.status',1)
                ->orderBy('created_at','desc')
                ->take(3)
                ->get();
        $rs['data'] = $list;
        return response()->json($rs);
    }

    public function notice(Request $request)
    {
        $rs['status'] = true;
        $where['status'] = 1;
        $where['cate_id'] = 1;
        $department = KfContent::where($where)->take(3)->orderBy('created_at','desc')->get();
        $rs['data'] = $department;
        return response()->json($rs);
    }

    public function getDoctorByDepartmentId(Request $request)
    {
        $rs['status'] = true;
        $department_id = $request->department_id;
        $data = DB::table('kf_doctor as a')
                    ->join('kf_doctor_department as b','a.id','=','b.doctor_id')
                    ->select('a.*')
                    ->where('b.department_id',$department_id)
                    ->get();
        $rs['data'] = $data;
        return response()->json($rs);
    }

    


    function a_array_unique($array)
    {
        $out = array();
        foreach ($array as $key=>$value) {
            if (!in_array($value, $out))
            {
                $out[$key] = $value;
            }
        }
        return $out;
    }


    //获取门诊详情
    function getOutpatient(Request $request)
    {
        $rs['status'] = true;
        $list = Kfoutpatient::select('id','name','img')->get()->toArray();
        $rs['data'] = $list;
        return response()->json($rs);
    }

    public function getOutpatientContent(Request $request)
    {
        $rs['status'] = true;
        $id = $request->outpatient_id;
        $data = Kfoutpatient::where('id',$id)->get()->toArray();
        foreach($data as $k=>&$v){
            $v['doctor'] = explode(',',$v['doctor']);
            foreach ($v['doctor'] as $kk=>$vv){
                $doctor = \DB::table('kf_doctor')->select('id','name','title','image','remark')->where('id',$vv)->get()->toArray();

                $v['doctor_name'][$kk] = $doctor[0];
            }
        }
        $rs['data'] = $data;
        return response()->json($rs);
    }

    public function getQuestion(Request $request)
    {
        $rs['status'] = true;
        $list = Kfquestion::select('id','name','img')->get()->toArray();
        $rs['data'] = $list;
        return response()->json($rs);
    }

    public function getAnswer(Request $request)
    {
        $rs['status'] = true;
        $id = $request->question_id;
        $data = Kfanswer::where('question_id',$id)->get()->toArray();
        $rs['data'] = $data;
        return response()->json($rs);
    }

    public function getAnswerContent(Request $request)
    {
        $rs['status'] = true;
        $id = $request->answer_id;
        $data = Kfanswer::where('id',$id)->get()->toArray();
        $rs['data'] = $data;
        return response()->json($rs);
    }

}
