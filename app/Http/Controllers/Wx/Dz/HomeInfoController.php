<?php

namespace App\Http\Controllers\Wx\Dz;


use App\Models\DaozhenHomeInfo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class HomeInfoController extends Controller
{
    
   
    

    /**
     * 文章详情
     */
    public function show(Request $request)
    {
        $rs['status'] = true;
        $where['status'] = 1;
        if($request->id) {
            $where['id'] = (int)$request->id;
        }
        $data = DaozhenHomeInfo::where($where)->first();
        $rs['data'] = $data;
        return response()->json($rs);
    }


    


}
