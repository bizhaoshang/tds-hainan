<?php

namespace App\Http\Controllers\Wx\Dz;


use App\Models\KfContent;
use App\Models\KfCate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ArticleController extends Controller
{
    
    /**
     * 就医指南用
     */
    public function articleCate(Request $request)
    {
        $rs['status'] = true;
        $where['pid'] = 5;          
        $list = KfCate::where($where)->get();
        $rs['data'] = $list;
        return response()->json($rs);
    }

    /**
     * 文章列表
     */
    public function articleList(Request $request)
    {
        $rs['status'] = true;
        $where['status'] = 1;
        if($request->cate_id) {
            $where['cate_id'] = (int)$request->cate_id;
        }
        $list = KfContent::where($where)->get();
        $rs['data'] = $list;
        return response()->json($rs);
    }

    /**
     * 文章详情
     */
    public function articleDetail(Request $request)
    {
        $rs['status'] = true;
        $where['status'] = 1;
        if($request->id) {
            $where['id'] = (int)$request->id;
        }
        $data = KfContent::where($where)->first();
        $rs['data'] = $data;
        return response()->json($rs);
    }


    


}
