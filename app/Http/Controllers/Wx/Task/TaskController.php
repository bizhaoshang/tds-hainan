<?php

namespace App\Http\Controllers\Wx\Task;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\WxRequest;
use App\Repos\TaskRepo;

class TaskController extends Controller
{
    public function index(WxRequest $request)
    {
        $rs['status'] = true;
        $where['staff_id'] = $request->staff_id;   
             
        $rs['data'] = TaskRepo::get($where);
        return response()->json($rs);
    }

    public function save(WxRequest $request)
    {
    	$rs['status'] = true;
    	$data = $request->pure();        
    	$flag = TaskRepo::save($data);
    	return response()->json($rs);
    }

    public function update(WxRequest $request)
    {
        $rs['status'] = true;
        $data = $request->pure();   
        $where['id']       = array_pull($data,'task_id');
        $where['staff_id'] = $request->staff_id;     
        $flag = TaskRepo::update($where,$data);
        return response()->json($rs);
    }

    public function info(WxRequest $request)
    {
    	$rs['status'] = true;
    	$where['id'] = $request->task_id;
    	$data 			   = TaskRepo::find($where);
    	$rs['data'] = $data;
    	return response()->json($rs);
    }

    /**
     * 派工
     */
    public function dispatchWorker(WxRequest $request)
    {
        $rs['status'] = true;
        $data = $request->all();
        $flag = TaskRepo::dispatch($data);
        $rs['data'] = $flag;
        return response()->json($rs);

    }

    public function complete(WxRequest $request)
    {
        $rs['status']      = true;
        $data = $request->pure();
        $data['worker_id'] = $request->staff_id;       
        $flag              = TaskRepo::complete($data);
        $rs['data'] = $flag;
        return response()->json($rs);
    }

    public function notice(WxRequest $request)
    {
        $rs['status']      = true;
        $rs['data'] = '这里是公告的内容';
        return response()->json($rs);
    }


    
}
