<?php

namespace App\Http\Controllers\Wx\Task;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ApiRequest;
use App\Models\Worker;

class WorkerController extends Controller
{
    public function index(ApiRequest $request)
    {
    	$rs['status'] = true;    	     
    	$rs['data']   = Worker::get();
    	return response()->json($rs);
    }

    
}
