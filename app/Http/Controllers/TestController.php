<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Kfsymptom;
use App\Models\KfTagSymptom;

class TestController extends Controller
{
    public function getByTag()
    {
    	$url = "http://cs.pro.atag.bsoft.com.cn/intelligenceserver/search/hotWordByType";
    	$client = new \GuzzleHttp\Client();

    	$targe['2']='1';
    	$targe['3']='2';
    	$targe['7']='3';
    	$targe['4']='4';
    	$targe['5']='5';

    	foreach($targe as $k=>$v) {
    		$response = $client->request('POST', $url, [
			    'form_params' => [
			        'id'     => $k,	       
			    ]
			]);
		
		
			$contents = $response->getBody()->getContents();
			$rs = json_decode($contents,true);	
			foreach($rs['data'] as $val){
				$data['name'] = $val;
				$s = Kfsymptom::updateOrCreate($data,$data);
				$ts['tag_id'] = $v;
				$ts['symptom_id'] = $s->id;
				KfTagSymptom::create($ts);


			}
			
    	}
		
		
    }
}
