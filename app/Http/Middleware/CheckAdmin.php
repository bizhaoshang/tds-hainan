<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use View;
use App\Models\Task;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        

        //验证管理员是否登录 如果未登录则跳转到登录页面
        if (!Session::has('admin')) {
            return redirect('zadmin/login');
        }


        $where['status'] = 1;
        $wait    = Task::where($where)->count();
        $where['status'] = 50;
        $process = Task::where($where)->count();
       

        View::share('task_wait',   $wait);
        View::share('task_process',$process);
        return $next($request);
    }
}
