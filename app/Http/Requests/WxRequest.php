<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Staff;

class WxRequest extends FormRequest
{
    public $staff_id;
    public $staff;

   

    
    public function authorize()
    {
        if(empty($this->token)) {
            return false;
        }
        $staff = Staff::where(['token'=>$this->token])->first();
        if($staff) {
            $this->staff_id = $staff->id;
            $this->staff    = $staff;
            return true;
        }        
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function messages()
    {
       return [];
    }

    

    public function pure()
    {
        $data = array_except($this->all(), ['token']);      
        return $data;
    }
}
