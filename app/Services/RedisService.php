<?php
namespace App\Services;
use Illuminate\Support\Facades\Redis;
use Cache,Log;

class RedisService
{
	public static function set($key,$data,$timestamp='')
	{
		
		try {
			Redis::set($key, $data);
			if($timestamp) {
				Redis::expireat($key,$timestamp);
			}		
		} catch(\Exception $e) {
			Log::error($e->getMessage());
		}
		return true;
			
	}

	public static function get($key)
	{
		return Redis::get($key);
	}

	

	public static function del($key)
	{
		return Redis::del($key);
	}
	
	public static function incr($key)
	{
		Redis::incr($key);
	}

	public static function decr($key)
	{
		Redis::decr($key);
	}

	public static function exists($key)
	{
		Redis::exists($key);
	}

	// lindex通过索引index获取列表的元素
	public static function lindex($key,$index=0)
	{
		return Redis::lindex($key,$index);
	}
	// 列表长度
	public static function llen($key)
	{
		return Redis::llen($key);
	}

	//入栈
	public static function lpush($key,$val)
	{
		Redis::lpush($key,$val);
	}

	// 出栈
	public static function lpop($key)
	{
		return Redis::lpop($key);
	}

	/**
	 * @param lng 127.909 ,lat 36.2323
	 */
	public static function geoaAddUser($lng,$lat,$name)
	{
		return Redis::geoadd("hainan:users:geo", $lng, $lat, $name);
	}

	public static function geoFindUser($lng,$lat,$dist=100)
	{
		return Redis::georadius("hainan:users:geo", $lng, $lat, $dist, "km", "WITHDIST", "ASC");
	}

	public static function addUser($key,$data)
	{
		return Redis::set('hainan:users:'.$key,$data);
	}

	public static function findUser($key)
	{
		return Redis::get('hainan:users:'.$key);
	}
		
}
?>