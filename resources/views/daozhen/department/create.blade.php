@extends('daozhen.layouts.app')
@include('vendor.ueditor.assets')
@section('content')
    <style>
      

    </style>
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">科室管理</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('zadmin/')}}">系统</a></li>
                <li><a href="{{route('department.index')}}">科室列表</a></li>
                <li class="active">新建科室</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h2 class="m-t-0 header-title">新建疾病</h2>
                <hr/>
                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-9">
                        <form class="form-horizontal" role="form" action="{{ url('daozhen/department')}}" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-md-3 control-label">科室名称</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="name" required="" value="{{ old('name') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">标签</label>
                                <div class="col-md-9" style="">
                                    @foreach($tag as $v)
                                        <label><input type="checkbox" name="tags[]"  value="{{$v->id}}">{{ $v->name }}</label>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">地址</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="href" required="" value="{{ old('name') }}">

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">排序</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="weight" required="" value="{{ old('weight') }}">

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">内容</label>
                                <div class="col-md-9">
                                    <script type="text/javascript">
                                        var ue = UE.getEditor('container');
                                        ue.ready(function() {
                                            ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
                                        });
                                    </script>

                                    <!-- 编辑器容器 -->
                                    <script id="container" name="contents" type="text/plain"></script>
                                </div>
                            </div>

                           
                            <div class="form-group text-center col-md-12">

                                <button type="submit" class="btn btn-info waves-effect waves-light">保存</button>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>

                </div>

            </div>

        </div> <!-- end col -->


    </div>
@endsection

@section('js')

    <script type="text/javascript">


        $('.type').on('change',function(){
            var _val = $(this).val();
            if(_val==2) {
                $('.money').show();
                $('.rate').hide();
            }else{
                $('.rate').show();
                $('.money').hide();
            }
        });
    </script>
@endsection

