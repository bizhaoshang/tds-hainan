@extends('daozhen.layouts.app')
@include('vendor.ueditor.assets')
@section('content')
    <style>


    </style>
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">门诊管理</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('zadmin/')}}">系统</a></li>
                <li><a href="{{route('outpatient.index')}}">门诊列表</a></li>
                <li class="active">新建门诊</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h2 class="m-t-0 header-title">新建门诊</h2>
                <hr/>
                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-9">
                        <form class="form-horizontal" role="form" action="{{ url('daozhen/outpatient')}}" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-md-3 control-label">门诊名称</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="name" required="" value="{{ old('name') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">关联医生</label>
                                <div class="col-md-9" style="">
                                    @foreach($doctor as $v)
                                        <label><input type="checkbox" name="doctor[]"  value="{{$v->id}}">{{ $v->name }}</label>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">banner图</label>
                                <div class="col-md-9">
                                    <input type="file" name="thumb" class="file" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">门诊图标</label>
                                <div class="col-md-9">
                                    <input type="file" name="img" class="file" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">内容</label>
                                <div class="col-md-9">
                                    <script type="text/javascript">
                                        var ue = UE.getEditor('container');
                                        ue.ready(function() {
                                            ue.execCommand('outpatient', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
                                        });
                                    </script>

                                    <!-- 编辑器容器 -->
                                    <script id="container" name="contents" type="text/plain"></script>
                                </div>
                            </div>


                            <div class="form-group text-center col-md-12">

                                <button type="submit" class="btn btn-info waves-effect waves-light">保存</button>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>

                </div>

            </div>

        </div> <!-- end col -->


    </div>
@endsection

@section('js')

    <script type="text/javascript">


        $('.type').on('change',function(){
            var _val = $(this).val();
            if(_val==2) {
                $('.money').show();
                $('.rate').hide();
            }else{
                $('.rate').show();
                $('.money').hide();
            }
        });
    </script>
@endsection

