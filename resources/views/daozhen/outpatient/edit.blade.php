@extends('daozhen.layouts.app')
@include('vendor.ueditor.assets')
@section('content')
    <style>
        .form-group::-webkit-scrollbar {
            display: none;
        }
    </style>
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">门诊管理</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('daozhen/')}}">系统</a></li>
                <li><a href="{{url('daozhen/outpatient')}}">门诊列表</a></li>
                <li class="active">修改门诊</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h2 class="m-t-0 header-title">修改门诊</h2>
                <hr/>
                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-9">
                        <form class="form-horizontal" role="form" action="{{ url('daozhen/outpatient',$outpatient->id) }}" method="post" enctype="multipart/form-data">{{ method_field('PUT') }}
                            <div class="form-group">
                                <label class="col-md-3 control-label">门诊名称</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="name" required="" value="{{$outpatient->name}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">banner图</label>
                                <div class="col-md-6">
                                    <input type="file"  name="thumb" class="file" >
                                </div>
                                <div class="col-md-3">
                                    <img style="width: 100px" src="{{ url('/').$outpatient->thumb }}" alt="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">门诊图标</label>
                                <div class="col-md-6">
                                    <input type="file"  name="img" class="file" >
                                </div>
                                <div class="col-md-3">
                                    <img style="width: 100px" src="{{ url('/').$outpatient->img }}" alt="">
                                </div>
                            </div>

                            <div class="form-group" >
                                <label class="col-md-3 control-label">医生列表</label>
                                <div class="col-md-9" style="">
                                    @foreach($doctor as $v)
                                        <label><input type="checkbox" name="doctor[]" {{ in_array($v->id,$doctor_id)  ? 'checked' : '' }} " value="{{$v->id}}">{{ $v->name }}</label>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">门诊介绍</label>
                                <div class="col-md-9">
                                    <script type="text/javascript">
                                        var ue = UE.getEditor('container');
                                        ue.ready(function() {
                                            ue.execCommand('outpatient', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
                                        });
                                    </script>
                                    <!-- 编辑器容器 -->
                                    <script id="container" name="contents" type="text/plain">{!!$outpatient->contents !!}</script>
                                </div>
                            </div>
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="form-group text-center col-md-12">
                                <button type="submit" class="btn btn-info waves-effect waves-light">保存</button>
                            </div>
                        </form>

                    </div>

                </div>
            </div>

        </div> <!-- end col -->


    </div>
@endsection






@section('modal')
    <!-- Modal -->

@endsection







@section('js')


@endsection


