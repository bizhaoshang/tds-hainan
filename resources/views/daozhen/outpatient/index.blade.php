@extends('daozhen.layouts.app')
@section('content')
    <style>
        #department::-webkit-scrollbar {
            display: none;
        }
    </style>
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">门诊管理</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('zadmin/')}}">系统</a></li>

                <li class="active">门诊列表</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-4">
                        <form role="form" href="{{ route('outpatient.index') }}">
                            <div class="form-group contact-search col-sm-8 m-b-30">
                                <input type="text" id="search" class="form-control"  name="name" value="{{ Request()->name ?? '' }}" placeholder="输入门诊名搜索">

                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                            </div> <!-- form-group -->
                        </form>
                    </div>



                    <div class="col-sm-7">


                        <a href="{{  url('daozhen/outpatient/create') }}" class="btn btn-primary btn-md waves-effect waves-light m-b-30"
                        ><i class="md md-add"></i>添加</a>



                    </div>
                </div>

                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif

                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>门诊名称</th>
                            <th>图片</th>
                            <th>对应医生</th>
                            <th>状态</th>

                            <th>操作</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($list as $v)
                            <tr>
                                <td>
                                    {{$v['id']}}
                                </td>
                                <td>
                                    {{$v['name']}}
                                </td>
                                <td>
                                    <img src="{{$v['thumb']}}" width="45" />
                                </td>
                                <td id="department">
                                    @foreach($v['doctor_name'] as $vv)
                                        <span class="btn btn-sm m-t-5 m-r-d btn-default btn-custom waves-effect waves-light">{{ $vv }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    状态
                                </td>
                                <td>
                                    <a href="{{ route('outpatient.edit',$v['id']) }}" ><i class="md md-edit"></i>编辑</a>
                                    <a type="submit" href="javascript:;" onclick="return delete_body('{{ $v['id'] }}')"  data-method="delete"
                                       data-token=""><i class="md md-close"></i>删除</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                </div>


            </div>

        </div> <!-- end col -->


    </div>
@endsection






@section('modal')

@endsection

@section('js')
    <script type="text/javascript">

        function delete_body(id) {
            if(!confirm('您确定要删除'+id)){
                return false;
            }

            $.post('{{ route('outpatient.delete') }}',{'id':id,'_token':'{{ csrf_token() }}','_method':'DELETE'},function (data) {
                if(data.code == 200){
                    alert(data.msg);
                    window.location.href = '{{ route('outpatient.index') }}';
                } else {
                    alert(data.msg);
                }
            },'json');

            return false;
        }


    </script>
@endsection









