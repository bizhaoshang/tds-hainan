@extends('daozhen.layouts.app')
@include('vendor.ueditor.assets')
@section('content')
    <style>
        .form-group::-webkit-scrollbar {
            display: none;
        }
    </style>
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">医生管理</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('daozhen/')}}">系统</a></li>
                <li><a href="{{url('daozhen/doctor')}}">医生列表</a></li>
                <li class="active">修改医生</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h2 class="m-t-0 header-title">修改医生</h2>
                <hr/>
                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-9">
                        <form class="form-horizontal" role="form" action="{{ url('daozhen/doctor',$doctor->id) }}" method="post" enctype="multipart/form-data">{{ method_field('PUT') }}
                            <div class="form-group">
                                <label class="col-md-3 control-label">大夫姓名</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="name" required="" value="{{$doctor->name}}">
                                </div>
                            </div>


                            <div class="form-group" >
                                <label class="col-md-3 control-label">推荐科室</label>
                                <div class="col-md-9" style="">
                                        @foreach($department as $v)
                                            <label><input type="checkbox" name="department[]" {{ in_array($v->id,$department_id)  ? 'checked' : '' }} " value="{{$v->id}}">{{ $v->name }}</label>
                                        @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">职位</label>
                                <div class="col-md-9">
                                    <script type="text/javascript">
    var ue = UE.getEditor('container');
    ue.ready(function() {
        ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
    });
</script>
<!-- 编辑器容器 -->
<script id="container" name="title" type="text/plain">{!!$doctor->title !!}</script>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">社会兼职</label>
                                <div class="col-md-9">
                                    <script type="text/javascript">
    var ue = UE.getEditor('containerJianzhi');
    ue.ready(function() {
        ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
    });
</script>
<!-- 编辑器容器 -->
<script id="containerJianzhi" name="jianzhi" type="text/plain">{!! $doctor->jianzhi !!}</script>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">医生简介</label>
                                <div class="col-md-9">
                                    <script type="text/javascript">
    var ue = UE.getEditor('containerContent');
    ue.ready(function() {
        ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
    });
</script>
<!-- 编辑器容器 -->
<script id="containerContent" name="content" type="text/plain">{!! $doctor->content !!}</script>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">专业特长</label>
                                <div class="col-md-9">
                                    <script type="text/javascript">
    var ue = UE.getEditor('containerRemark');
    ue.ready(function() {
        ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
    });
</script>
<!-- 编辑器容器 -->
<script id="containerRemark" name="remark" type="text/plain">{!! $doctor->remark !!}</script>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">医疗宣教</label>
                                <div class="col-md-9">
                                     <script type="text/javascript">
    var ue = UE.getEditor('containerWorktime');
    ue.ready(function() {
        ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
    });
</script><!-- 编辑器容器 -->
<script id="containerTeach" name="teach" type="text/plain">{!! $doctor->teach !!}</script>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">坐诊时间</label>
                                <div class="col-md-9">
                                     <script type="text/javascript">
    var ue = UE.getEditor('containerTeach');
    ue.ready(function() {
        ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
    });
</script>
<!-- 编辑器容器 -->
<script id="containerWorktime" name="worktime" type="text/plain">{!! $doctor->worktime !!}</script>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">头像</label>
                                <div class="col-md-6">
                                    <input type="file"  name="img" class="file" >
                                </div>
                                <div class="col-md-3">
                                    <img style="width: 100px" src="{{ url('/').$doctor->image }}" alt="">
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="col-md-3 control-label">排序</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="weight"  value="{{ $doctor->weight}}"/>
                                </div>
                            </div>
                            
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="form-group text-center col-md-12">
                                <button type="submit" class="btn btn-info waves-effect waves-light">保存</button>
                            </div>
                        </form>

                    </div>

                </div>
            </div>

        </div> <!-- end col -->


    </div>
@endsection






@section('modal')
    <!-- Modal -->
    <div id="custom-modal" class="modal-demo">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only">Close</span>
        </button>
        <h4 class="custom-modal-title">账户充值</h4>
        <div class="custom-modal-text text-left">
            <form role="form" method="post" action="{{url('daozhen/disease/add-money')}}">

                <div class="form-group">
                    <label for="position">卖家</label>
                    <input type="text" class="form-control" id="seller_name" disabled="" />
                </div>
                <div class="form-group">
                    <label for="name">充值金额</label>
                    <input type="number" class="form-control" name="money" placeholder="">
                </div>



                <div class="form-group">
                    <label for="position">备注信息</label>
                    <textarea class="form-control" name="remark" ></textarea>
                </div>


                <button type="submit" class="btn btn-default waves-effect waves-light">保存</button>
                <button type="button" class="btn btn-danger waves-effect waves-light m-l-10" onclick="Custombox.close();">取消</button>

                <input type="hidden" name="seller_id" />
                {{csrf_field()}}
            </form>
        </div>
    </div>
@endsection







@section('js')

    <script type="text/javascript">


        $('.type').on('change',function(){
            var _val = $(this).val();
            if(_val==2) {
                $('.money').show();
                $('.rate').hide();
            }else{
                $('.rate').show();
                $('.money').hide();
            }
        });
    </script>
@endsection


