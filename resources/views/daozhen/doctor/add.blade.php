@extends('daozhen.layouts.app')
@include('vendor.ueditor.assets')
@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">医生管理</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('daozhen/')}}">系统</a></li>
                <li><a href="{{url('daozhen/doctor')}}">医生列表</a></li>
                <li class="active">新建医生</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h2 class="m-t-0 header-title">新建疾病</h2>
                <hr/>
                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-9">
                        <form class="form-horizontal" role="form" action="{{ url('daozhen/doctor')}}" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-md-3 control-label">医生名称</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="name" required="" value="{{ old('name') }}">
                                </div>
                            </div>

                            <div class="form-group" >
                                <label class="col-md-3 control-label">标签</label>
                                <div class="col-md-9" style="">
                                    @foreach($department as $v)
                                        <label><input type="checkbox" name="department[]" style="flaot:left;margin:4px" value="{{$v->id}}">{{ $v->name }}</label>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">职位</label>
                                <div class="col-md-9">
<script type="text/javascript">
    var ue = UE.getEditor('container');
    ue.ready(function() {
        ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
    });
</script>
<!-- 编辑器容器 -->
<script id="container" name="title" type="text/plain">{!! old('title') !!}</script>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">社会兼职</label>
                                <div class="col-md-9">
<script type="text/javascript">
    var ue = UE.getEditor('containerJianzhi');
    ue.ready(function() {
        ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
    });
</script>
<!-- 编辑器容器 -->
<script id="containerJianzhi" name="jianzhi" type="text/plain">{!! old('jianzhi') !!}</script>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">医生简介</label>
                                <div class="col-md-9">
                                    <script type="text/javascript">
    var ue = UE.getEditor('containerContent');
    ue.ready(function() {
        ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
    });
</script>
<!-- 编辑器容器 -->
<script id="containerContent" name="content" type="text/plain">{!! old('content') !!}</script>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">专业特长</label>
                                <div class="col-md-9">
                                     <script type="text/javascript">
    var ue = UE.getEditor('containerRemark');
    ue.ready(function() {
        ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
    });
</script>
<!-- 编辑器容器 -->
<script id="containerRemark" name="remark" type="text/plain">{!! old('remark') !!}</script>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">坐诊时间</label>
                                <div class="col-md-9">
                                     <script type="text/javascript">
    var ue = UE.getEditor('containerWorktime');
    ue.ready(function() {
        ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
    });
</script>
<!-- 编辑器容器 -->
<script id="containerWorktime" name="worktime" type="text/plain">{!! old('worktime') !!}</script>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 control-label">头像</label>
                                <div class="col-md-9">
                                    <input type="file" name="image" class="file" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">排序</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="weight"  value="{{ old('weight') }}"/>
                                </div>
                            </div>
                            <div class="form-group text-center col-md-12">

                                <button type="submit" class="btn btn-info waves-effect waves-light">保存</button>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>

                </div>




            </div>

        </div> <!-- end col -->


    </div>
@endsection












@section('js')

    <script type="text/javascript">


    </script>
@endsection

