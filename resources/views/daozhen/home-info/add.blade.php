@extends('daozhen.layouts.app')
@include('vendor.ueditor.assets')
@section('css')
    <link href="{{asset('admin/plugins/select2/css/select2.css')}}" rel="stylesheet">
@endsection
@section('content')


    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">内容管理</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('daozhen/')}}">系统</a></li>
                <li><a href="{{ url('daozhen/home-info') }}">内容列表</a></li>
                <li class="active">添加内容</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h2 class="m-t-0 header-title">添加内容</h2>
                <hr/>
                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-9">
                        <form class="form-horizontal" role="form" action="{{ url('daozhen/home-info') }}" method="post" enctype="multipart/form-data">


                <div class="form-group">
                    <label class="col-md-2 control-label">标题</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="title" value="">
                    </div>
                </div>

               
  
<div class="form-group">
                  <label class="col-md-2 control-label">内容</label>
                  <div class="col-md-10">

                            <script type="text/javascript">
                                var ue = UE.getEditor('container');
                                ue.ready(function() {
                                    ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
                                });
                            </script>
                            <!-- 编辑器容器 -->
                            <script id="container" name="contents" type="text/plain"></script>
</div>
</div>

<div class="form-group">
                  <label class="col-md-2 control-label">状态</label>
                  <div class="col-md-10">
            <input type="radio" name="status" value="1"  checked="">开启
                      <input type="radio" name="status" value="0" >
                      关闭
                  </div>
              </div>

           



                            {{csrf_field()}}
                            <div class="form-group text-center col-md-12">
                                <button type="submit" class="btn btn-info waves-effect waves-light">保存</button>
                            </div>
                        </form>

                    </div>

                </div>
            </div>

        </div> <!-- end col -->


    </div>
@endsection






@section('modal')
   
   
@endsection







@section('js')
<script type="text/javascript" src="{{asset('admin/plugins/select2/js/select2.min.js')}}"></script>
    <script type="text/javascript">

            $('#department_ids').select2(); 
    </script>
@endsection

