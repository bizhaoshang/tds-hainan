@extends('daozhen.layouts.app')
@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">公告管理</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('zadmin/')}}">系统</a></li>

                <li class="active">公告列表</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">

                <div class="row">
                    <div class="col-sm-4">
                        <form role="form" href="{{ route('department.index') }}">
                            <div class="form-group contact-search col-sm-8 m-b-30">
                                <input type="text" id="search" class="form-control"  name="name" value="{{ Request()->name ?? '' }}" placeholder="输入标题搜索">

                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                            </div> <!-- form-group -->
                        </form>
                    </div>



                    <div class="col-sm-7">

                       
                            <a href="{{  url('daozhen/home-info/create') }}" class="btn btn-primary btn-md waves-effect waves-light m-b-30"
                            ><i class="md md-add"></i>添加</a>
                       


                    </div>
                </div>
                
                <hr/>
                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif

                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>标题</th>
                            
                            <th>添加时间</th>
                            <th>开关</th>
                            <th style="width: 200px;">操作</th>
                        </tr>
                        </thead>

                        <tbody>
                        
                            @foreach($list as $k =>$v)
                            <tr>
                                <td>{{$v->id}}</td>
                                <td>
                                    {{$v->title}}
                                </td>
                                
                                
                                <td>
                                   {{$v->created_at}}
                                </td>
                                <td>
                <span class="btn btn-sm btn-{{$v->status==1?'success':'danger'}}">{{$v->status==1?'开启':'关闭'}}</span>  
                            
                                </td>

                                        <td>
    <a href="{{ url('daozhen/home-info/'.$v->id.'/edit') }}" class="btn btn-sm btn-info" >编辑</a>
    <a href="{{url('daozhen/home-info',$v->id)}}" class="btn btn-sm btn-danger" data-method="delete" 
  data-token="{{csrf_token()}}" data-confirm="确定删除吗?">删除</a>
                                           
                                        </td>
                               

                            </tr>
                            @endforeach
                         







                        </tbody>
                    </table>


                </div>
                 {{ $list->links() }}

                </div>

        </div> <!-- end col -->


    </div>

@endsection



@section('modal')
   
@endsection

@section('js')
    <script type="text/javascript">
        $('#checkAll').click(function () {
            $('input:checkbox').prop('checked', this.checked);
        });


        $('.bathDel').on('click',function(){
            var ids = $('.check_class:checked').map(function() {
                return this.value;
            }).get();
            if(ids==''){
                alert('请选择要删除的数据');return;
            }
            var url="{{ route('body.deleteAll') }}";
            var data = {
                'ids':ids,
                '_method':'DELETE',
                '_token' :'{{ csrf_token() }}'
            };
            // data.ids = ids;
            {{--data._token = "{{csrf_token()}}";--}}
            $.post(url,data,function(data){
                // if(data,code == 200) {
                window.location.reload();
                // }
            },'json');
            // console.info(ids);
        });

        function display(id) {
            if($('.son_'+id).attr('hidden') == 'hidden'){
                $('.son_'+id).removeAttr('hidden');
            } else {
                $('.son_'+id).attr('hidden',true);
            }
        }

       


        
    </script>
@endsection
