@extends('daozhen.layouts.app')
@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">疾病管理</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('daozhen/')}}">系统</a></li>
                <li><a href="{{url('daozhen/disease')}}">疾病列表</a></li>
                <li class="active">关联症状</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h2 class="m-t-0 header-title">关联症状</h2>
                <hr/>
                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-9">
                        <form class="form-horizontal" role="form" action="{{url('daozhen/disease/symptom/insertdata/'.$data->id)}}" method="post" enctype="multipart/form-data">

                            <div class="form-group">
                                <label class="col-md-3 control-label">疾病名称</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="name" required="" value="{{$data->name}}">
                                </div>
                            </div>


                            <div class="form-group"  id="public">
                                <label class="col-md-3 control-label">症状选择</label>
                               
                                        <div class="col-md-9" >
                                            @foreach($result as $k=>$v)
                                            <div class="col-md-4 m-b-10" >
                                                <div class="col-md-6">    
                                                <input type="checkbox" name="symptom_id[]" value="{{$v->id}}" {{in_array($v->id,$symptom_ids) ? 'checked=""' : ''}}  />
                                                {{$v->name}}
                                                </div>
                                                <div class="col-md-6">
                                                <input type="text" class="form-control" name="probability[]" placeholder="概率" value="{{array_get($probabilitys,$v->id)}}">
                                                </div>
                                            </div>     
                                            @endforeach
                                        </div>
                                  
                            </div>



                          

                         


                           

                            <div class="form-group text-center col-md-12">

                                <button type="submit" class="btn btn-info waves-effect waves-light">保存</button>
                            </div>

                            {{csrf_field()}}
                        </form>
                    </div>

                </div>




            </div>

        </div> <!-- end col -->


    </div>
@endsection






@section('modal')
    <!-- Modal -->
    
    </div>
@endsection







@section('js')

    <script type="text/javascript">


        $('.type').on('change',function(){
            var _val = $(this).val();
            if(_val==2) {
                $('.money').show();
                $('.rate').hide();
            }else{
                $('.rate').show();
                $('.money').hide();
            }
        });


        // $('.public_div').each(function (){
        //     console.log($(this).attr('hidden',true));
        //     if($(this).children().eq(0).attr('value') <50){
        //    }
        // });
        $('#search').blur(function() {
            var name = $(this).val();
            if(name == ''){
                public_div(name);
                return;
            }
            $.ajax({
                url: '{{ route('disease.search') }}',
                type: 'POST',
                dataType: 'json',
                data:{'name':name,'_token':'{{ csrf_token() }}'},
                success: function(msg) {
                   if(msg.code == 200){
                       // console.log(msg.data);
                       $('#search').val(msg.data2.name);
                       public_div(msg);
                   } else {
                        alert('没有匹配到该疾病');
                   }
                },
                error: function(xhr, type) {

                }
            })
        });




        $('.tags').click(function(){
            var id = $(this).attr('id');
            if(id == '') {
                public_div(id);
                return;
            }
            $.ajax({
                url: '{{ route('disease.tag_search') }}',
                type: 'POST',
                dataType: 'json',
                data:{'id':id,'_token':'{{ csrf_token() }}'},
                success: function(msg) {
                    if(msg.code == 200){
                        // console.log(msg.data);
                        // $('#search').val(msg.data2.name);
                        public_div(msg);
                    } else {
                        alert('没有匹配到该疾病');
                    }
                },
                error: function(xhr, type) {

                }
            })

        })


        function public_div(msg){
            if(msg != '') {
                $('.public_div').each(function (){
                    // console.log($(this).attr('hidden',true));
                    $(this).css('color','');
                    for(var i=0;i<msg.data.length;i++){
                        // console.log(msg.data[i]);
                        if( $(this).children().eq(0).attr('value') == msg.data[i]) {
                            $(this).css('color','red');
                            // $(this).children().eq(0).attr('backgroud','#ddd');
                        }
                    }
                    //
                });
            } else {
                $('.public_div').each(function (){
                    // console.log($(this).attr('hidden',true));
                    $(this).css('color','');
                    //
                });
            }

        }
    </script>
@endsection

