@extends('daozhen.layouts.app')
@include('vendor.ueditor.assets')
@section('content')
    <style>
        .form-group::-webkit-scrollbar {
            display: none;
        }
    </style>
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">就医指南</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('daozhen/')}}">系统</a></li>
                <li><a href="{{url('daozhen/question')}}">问题列表</a></li>
                <li class="active">修改问题</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h2 class="m-t-0 header-title">修改问题</h2>
                <hr/>
                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-9">
                        <form class="form-horizontal" role="form" action="{{ url('daozhen/question',$question->id) }}" method="post" enctype="multipart/form-data">{{ method_field('PUT') }}
                            <div class="form-group">
                                <label class="col-md-3 control-label">问题名称</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="name" required="" value="{{$question->name}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">图标</label>
                                <div class="col-md-6">
                                    <input type="file"  name="img" class="file" >
                                </div>
                                <div class="col-md-3">
                                    <img style="width: 100px" src="{{ url('/').$question->img }}" alt="">
                                </div>
                            </div>
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="form-group text-center col-md-12">
                                <button type="submit" class="btn btn-info waves-effect waves-light">保存</button>
                            </div>
                        </form>

                    </div>

                </div>
            </div>

        </div> <!-- end col -->


    </div>
@endsection






@section('modal')
    <!-- Modal -->

@endsection







@section('js')


@endsection


