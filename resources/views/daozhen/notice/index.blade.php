@extends('admin.layouts.app')
@section('css')
	<link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
 
@endsection
@section('content')


						<!-- Page-Title -->
						<div class="row">
							<div class="col-sm-12">
                                

								<h4 class="page-title">管理后台</h4>
								<ol class="breadcrumb">
									<li>
										<a href="{{url('zadmin/')}}">塔台</a>
									</li>
									<li class="active">
										<a href="{{url('zadmin/qas')}}">常见问题管理</a>
									</li>
									
								</ol>
							</div>
						</div>
						

						
						<div class="row">
	<div class="col-sm-12">
								<div class="card-box">
                            				
        <form class="form-inline">
			<div class="form-group m-r-10">
			<label for="exampleInputName2">关键词</label>
				<input type="text" class="form-control" name="name" placeholder="" value="{{array_get($where,'name')}}">
			</div>
			
			
			<button type="submit" class="btn btn-default waves-effect waves-light btn-md">
				搜索
			</button>
			<a href="{{url('zadmin/qas/create')}}" class="btn btn-info waves-effect waves-light btn-md">
				添加
			</a>
		</form>
							</div>				
                         </div>

							<div class="col-sm-12">
								<div class="card-box">

@include('admin.common.error')
									
		<div class="table-rep-plugin">
			<div class="table-responsive" >
				<table  class="table  table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>标题</th>
							<th>内容</th>
							<th>排序</th>
							<th>添加时间</th>							
							<th data-priority="6">操作</th>
						</tr>
					</thead>
					<tbody>
                        @foreach($list as $v)  
						<tr>
							<th>{{$v->id}}</th>
							
							<td>{{$v->title}}</td>
							
						
							<td>{!!$v->contents!!}</td>
														
							<td>
								
								{{$v->weight}}
								
							</td>
							
							
							
							<td>{{$v->created_at}}</td>
							

							
							<td>
								<a href="{{url('zadmin/qas/'.$v->id.'/edit')}}" class="btn btn-primary btn-sm">修改</a>
							

							<a class="btn btn-sm btn-danger" href="{{url('zadmin/qas/delete',$v->id)}}" data-method="delete" 
  data-token="{{csrf_token()}}" data-confirm="确定删除吗?">
<span class="btn-label">
  <i class="md md-close"></i>
</span>删除</a>
							</td>
						</tr>
                        @endforeach
													
												</tbody>
											</table>
										</div>
{{$list->appends($where)->links()}}
									</div>

								</div>
							</div>
						</div>



<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          &times;
        </button>
        <h4 class="modal-title" id="myModalLabel">
          操作
        </h4>
      </div>
       <form method="post" action="{{url('zadmin/qas/sale-status')}}" >
      <div class="modal-body">
             
            <div class="form-group row">
                <label class="col-md-4 control-label">备注</label>
                <div class="col-md-8">
                  <textarea class="form-control" rows="5" name="remark" placeholder="备注"></textarea>
                </div>
            </div>
            <input type="hidden" name="id" id="product_id" value="" /> 
            <input type="hidden" name="status" id="status" value="">
       {{csrf_field()}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭
        </button>
        <button type="submit" class="btn btn-primary">
          确定
        </button>
      </div>
    </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal -->
</div>
 @endsection 
               
         

    @section('js')
        <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}" type="text/javascript"></script>

      <script type="text/javascript">
      	var product_id = '';
      	var status = '';
      	$('.saleStatus').on('click',function(){
      		
      		product_id = $(this).data('id');
      		status     = $(this).data('status');
      		$('#product_id').val(product_id);
      		$('#status').val(status);
      		$('#statusModal').modal('show');
      	});
      </script>
    @endsection    

