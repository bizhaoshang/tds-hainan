@extends('admin.layouts.app')
@section('css')

@endsection
@section('content')
@include('vendor.ueditor.assets')

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">平台管理</h4>
                                <ol class="breadcrumb">
                                    <li><a href="{{url('zadmin/')}}">系统</a></li>
                                    <li><a href="{{url('zadmin/qas')}}">声明/协议管理</a></li>
                                    <li class="active">修改</li>
                                </ol>
                            </div>
                        </div>
                        
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="card-box">
                            <h2 class="m-t-0 header-title">修改{{$data->title}}</h2>
                            <hr/>
                             
                              @include('admin.common.error')
                              <div class="row">
                                <div class="col-md-9">
                                  <form class="form-horizontal" role="form" action="{{url('zadmin/notice-update')}}" method="post">

                      <div class="form-group">
                          <label class="col-md-2 control-label">标题</label>
                          <div class="col-md-10">
                            <input type="text" class="form-control" name="title" required="" value="{{$data->title}}" >
                          </div>
                      </div>
                      

                                             
              <div class="form-group">
              <label class="col-md-2 control-label">内容</label>
              <div class="col-md-10">
               <!-- 实例化编辑器 -->
<script type="text/javascript">
    var ue = UE.getEditor('container');
    ue.ready(function() {
        ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
    });
</script>

<!-- 编辑器容器 -->
<script id="container" name="contents" type="text/plain">{!!$data->contents!!}</script>
              </div>
            </div> 
         

          
               
                  


  
                                              

                                              

        <div class="form-group text-center">
                                                  
          <button type="submit" class="btn btn-primary waves-effect waves-light">保存</button>

        </div>
                                              
        <input name="id" value="{{$data->id}}" type="hidden" />                                      
                             {{csrf_field()}}
                             
                    </form>
                                </div>
                                
                              </div>

                             
                              
                              
                            </div>
                                
                            </div> <!-- end col -->

                            
                        </div>
@endsection
@section('modal')  

     
    @endsection                         

    
  @section('js')
  <script type="text/javascript" src="/js/plupload.full.min.js"></script>
  <script type="text/javascript" src="{{asset('assets/plugins/switchery/js/switchery.min.js')}}"></script>

        <script type="text/javascript">
         
        </script>
        
        @endsection
    
