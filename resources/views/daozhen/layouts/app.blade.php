<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Coderthemes">

        

        <title>{{env('APP_NAME')}}</title>

        @yield('css')
        <link href="{{asset('purple/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('purple/assets/css/core.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('purple/assets/css/components.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('purple/assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('purple/assets/css/pages.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('purple/assets/css/responsive.css')}}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="{{asset('purple/assets/js/modernizr.min.js')}}"></script>

        
        
    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            @include('daozhen.layouts.top')


            <!-- ========== Left Sidebar Start ========== -->

            @include('daozhen.layouts.left')
            <!-- Left Sidebar End --> 



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                     

                         @yield('content')
                        
                        

                    </div> <!-- container -->
                               
                </div> <!-- content -->

              

            </div>
            
            
        <!-- Modal -->
        @yield('modal')
            
       

           

        </div>
        <!-- END wrapper -->


    
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
         <script src="{{asset('purple/assets/js/jquery.min.js')}}"></script>
        <script src="{{asset('purple/assets/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('purple/assets/js/detect.js')}}"></script>
        <script src="{{asset('purple/assets/js/fastclick.js')}}"></script>
        <script src="{{asset('purple/assets/js/jquery.slimscroll.js')}}"></script>
        <script src="{{asset('purple/assets/js/jquery.blockUI.js')}}"></script>
        <script src="{{asset('purple/assets/js/waves.js')}}"></script>
        <script src="{{asset('purple/assets/js/wow.min.js')}}"></script>
        <script src="{{asset('purple/assets/js/jquery.nicescroll.js')}}"></script>
        <script src="{{asset('purple/assets/js/jquery.scrollTo.min.js')}}"></script>

       
        
        

            @yield('js')
        

        

        <script src="{{asset('purple/assets/js/jquery.core.js')}}"></script>
        <script src="{{asset('purple/assets/js/jquery.app.js')}}"></script>

        <script type="text/javascript" src="{{asset('js/laravel.js')}}"></script>
            

    
    </body>
</html>