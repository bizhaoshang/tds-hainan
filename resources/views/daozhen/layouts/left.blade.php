      <!-- ========== Left Sidebar Start ========== -->

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>

                           

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-home"></i> <span> 塔台 </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="{{url('zadmin/')}}">首页</a></li>
                                    
                                </ul>
                            </li>

                            <li>
                                <a href="{{url('daozhen/disease')}}" class="waves-effect"><i class="ti-user"></i> <span> 疾病管理 </span> <span class="menu-arrow"></span> </a>
                                
                            </li>

                            
                            <li>
                                <a href="{{url('daozhen/symptom')}}" class="waves-effect"><i class="md md-devices"></i> <span> 症状管理 </span> <span class="menu-arrow"></span> </a>
                               
                            </li>



                            <li>
                                <a href="{{url('daozhen/body')}}" class="waves-effect"><i class="ti ti-ticket"></i> <span> 身体部位 </span> <span class="menu-arrow"></span> </a>
                                
                            </li>

                            <li class="has_sub">
                                <a href="{{url('daozhen/tags')}}" class="waves-effect"><i class="ti-notepad"></i> <span>Tags </span> <span class="menu-arrow"></span> </a>
                                
                            </li>

           

                            
                            <li>
                                <a href="{{url('daozhen/doctor')}}" class="waves-effect"><i class="md  md-directions-car"></i><span> 医生管理 </span> <span class="menu-arrow"></span></a>
                               
                            </li>

                            

                            <li>
                                <a href="{{url('daozhen/department')}}" class="waves-effect"><i class="md md-group-work"></i> <span> 科室管理 </span> <span class="menu-arrow"></span> </a>
                                
                            </li>
                            <li>
                                <a href="{{url('daozhen/content')}}" class="waves-effect"><i class="md md-subtitles"></i><span>内容管理</span> <span class="menu-arrow"></span></a>
                               
                            </li>
                            <li>
                                <a href="{{url('daozhen/outpatient')}}" class="waves-effect"><i class="md md-devices"></i> <span> 门诊管理 </span> <span class="menu-arrow"></span> </a>

                            </li><li>
                                <a href="{{url('daozhen/question')}}" class="waves-effect"><i class="md md-devices"></i> <span> 就医指南 </span> <span class="menu-arrow"></span> </a>
                            </li></li>
                            <li>
                                <a href="{{url('daozhen/answer')}}" class="waves-effect"><i class="md md-devices"></i> <span> 问题解答 </span> <span class="menu-arrow"></span> </a>
                            </li>
                             <li>
                                <a href="{{url('daozhen/home-info')}}" class="waves-effect"><i class="md md-devices"></i> <span> 首页富文本 </span> <span class="menu-arrow"></span> </a>
                            </li>

                            
           
               
                         
  
        

                        

                           
                            

                           

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End -->