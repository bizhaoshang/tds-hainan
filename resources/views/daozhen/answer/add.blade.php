@extends('daozhen.layouts.app')
@include('vendor.ueditor.assets')
@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">问题解答</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('daozhen/')}}">系统</a></li>
                <li><a href="{{url('daozhen/answer')}}">问题解答</a></li>
                <li class="active">新建答疑</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h2 class="m-t-0 header-title">新建答疑</h2>
                <hr/>
                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-9">
                        <form class="form-horizontal" role="form" action="{{ url('daozhen/answer')}}" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-md-3 control-label">问题种类</label>
                                <div class="col-md-9">
                                    <select  class="form-control" name="question_id" required="">
                                        <option value="">请选择</option>
                                        @foreach($question as $v)
                                            <option value="{{$v->id}}">{{$v->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">解答详情</label>
                                <div class="col-md-9">
                                    <script type="text/javascript">
                                        var ue = UE.getEditor('container');
                                        ue.ready(function() {
                                            ue.execCommand('answer', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
                                        });
                                    </script>
                                    <!-- 编辑器容器 -->
                                    <script id="container" name="text" type="text/plain">{!! old('text') !!}</script>
                                </div>
                            </div>
                            <div class="form-group text-center col-md-12">

                                <button type="submit" class="btn btn-info waves-effect waves-light">保存</button>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>

                </div>




            </div>

        </div> <!-- end col -->


    </div>
@endsection












@section('js')

    <script type="text/javascript">


    </script>
@endsection

