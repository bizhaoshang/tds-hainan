@extends('daozhen.layouts.app')
@section('content')
    <style>
        #department::-webkit-scrollbar {
            display: none;
        }
    </style>
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">问题解答</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('zadmin/')}}">系统</a></li>

                <li class="active">问题解答</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-4">
                        <form role="form" href="{{ route('answer.index') }}">
                            <div class="form-group contact-search col-sm-8 m-b-30">
                                <input type="text" id="search" class="form-control"  name="name" value="{{ Request()->name ?? '' }}" placeholder="输入问题名称搜索">

                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                            </div> <!-- form-group -->
                        </form>
                    </div>



                    <div class="col-sm-7">


                        <a href="{{  url('daozhen/answer/create') }}" class="btn btn-primary btn-md waves-effect waves-light m-b-30"
                        ><i class="md md-add"></i>添加</a>



                    </div>
                </div>

                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif

                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>标题</th>
                            <th>关联问题</th>
                            <th>状态</th>
                            <th>操作</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($list as $v)
                            <tr>
                                <td>
                                    {{$v->id}}
                                </td>
                                <td>
                                    {{$v->title}}
                                </td>
                                <td>
                                        <span class="btn btn-sm m-t-5 m-r-d btn-default btn-custom waves-effect waves-light">{{ $v->question->name }}</span>
                                </td>
                                <td>
                                    状态
                                </td>
                                <td>
                                    <a href="{{ route('answer.edit',$v->id) }}" ><i class="md md-edit"></i>编辑</a>
                                    <a type="submit" href="javascript:;" onclick="return delete_body('{{ $v->id}}')"  data-method="delete"
                                       data-token=""><i class="md md-close"></i>删除</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                </div>


            </div>

        </div> <!-- end col -->


    </div>
@endsection






@section('modal')

@endsection

@section('js')
    <script type="text/javascript">

        function delete_body(id) {
            if(!confirm('您确定要删除'+id)){
                return false;
            }

            $.post('{{ route('answer.delete') }}',{'id':id,'_token':'{{ csrf_token() }}','_method':'DELETE'},function (data) {
                if(data.code == 200){
                    alert(data.msg);
                    window.location.href = '{{ route('answer.index') }}';
                } else {
                    alert(data.msg);
                }
            },'json');

            return false;
        }


    </script>
@endsection









