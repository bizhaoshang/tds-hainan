@extends('daozhen.layouts.app')
@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">症状管理</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('daozhen/')}}">系统</a></li>

                <li class="active">症状列表</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-md-8">
                        <form role="form">
                            <div class="form-group contact-search col-sm-8 m-b-30">
                                <input type="text" id="search" class="form-control"  name="keyword" value="{{array_get($where,'keyword')}}" placeholder="输入症状名搜索">

                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                            </div> <!-- form-group -->
                        </form>
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-white" href="{{ route('symptom.index') }}">刷新</a>
                    </div>
                    <div class="col-md-2">

                        <a href="{{url('daozhen/symptom/create')}}" class="btn btn-primary btn-md waves-effect waves-light m-b-30"
                                ><i class="md md-add"></i>添加</a>
                           


                    </div>
                </div>
                <div class="row">    
                    
                    @foreach($tags as $v)
                    <div class="col-sm-2 m-t-10">
                        <a href="{{ url('/daozhen/symptom') }}?tag={{ $v->id }}" class="btn btn-success {{array_get($where,'tag')==$v->id?'':'btn-custom'}} waves-effect waves-light">
                            {{ $v->name }}
                            ({{  $v->tagSymptom->count() }})
                        </a>
                    </div>
                    @endforeach
                    <div class="col-sm-2 m-t-10">
                        <a href="{{ url('/daozhen/symptom') }}" class="btn btn-info btn-custom waves-effect waves-light">全部({{$total}})</a>
                    </div>
                    

                    
                </div>
                <hr/>
                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif

               
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th style="min-width: 35px;">
                                <input id="checkAll" type="checkbox" value=""/>全选

                            </th>
                            <th>症状名称</th>
                            <th>症状类别</th>



                            <th style="width: 200px;">操作</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($list as $k=>$v)
                            <tr>
                                <td>

                                    <input  name="ids[]" type="checkbox" value="{{$v->id}}" class="check_class">
                                    {{($start+$k)}}

                                </td>

                                <td>
                                    {{$v->name}}
                                </td>
                                <td>
                                    @foreach($v->tags as $vv)
                                        <span class="btn btn-sm m-t-5 m-r-d btn-white btn-custom waves-effect waves-light">{{ $vv->name }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{url('daozhen/symptom/'.$v->id.'/edit')}}" ><i class="md md-edit"></i>编辑</a>
                                    <a href="{{url('daozhen/symptom',$v->id)}}" data-method="delete"
                                       data-token="{{csrf_token()}}" data-confirm="确定删除吗?"><i class="md md-close"></i>删除</a>
                                </td>
                            </tr>

                        @endforeach







                        </tbody>
                    </table>

                    <button type="button" class="btn-danger bathDel">批量删除</button>

                </div>

                {{$list->appends($where)->links()}}
            </div>

        </div> <!-- end col -->


    </div>
@endsection






@section('modal')
    
@endsection

@section('js')
    <script type="text/javascript">
        $('#checkAll').click(function () {
            $('input:checkbox').prop('checked', this.checked);
        });


        $('.bathDel').on('click',function(){
            var ids = $('.check_class:checked').map(function() {
                return this.value;
            }).get();
            if(ids==''){
                alert('请选择要删除的数据');return;
            }
            var url="{{url('daozhen/symptom/bath-del')}}";
            var data = {};
            data.ids = ids;
            data._token = "{{csrf_token()}}";
            console.log(url);
            $.post(url,data,function(rs){

                if(rs.status==true) {
                    window.location.reload();
                }
            });
            console.info(ids);
        });



    </script>
@endsection









