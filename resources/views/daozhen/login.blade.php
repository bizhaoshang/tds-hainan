<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
       

        
        <title>{{env('APP_NAME')}}</title>

        <link href="{{asset('purple/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('purple/assets/css/core.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('purple/assets/css/components.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('purple/assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('purple/assets/css/pages.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('purple/assets/css/responsive.css')}}" rel="stylesheet" type="text/css" />

        
        
    </head>
    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
        	<div class=" card-box">
            <div class="panel-heading"> 
                <h3 class="text-center">  <strong class="text-custom">系统登陆</strong> </h3>
            </div> 

    @if(session('rs'))
    <div class="alert alert-danger">
    {{ session('rs')['msg'] }}
    </div>
    @endif
            <div class="panel-body">
            <form class="form-horizontal m-t-20" method="post" action="{{url('daozhen/login-do')}}">
                
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" name="username" placeholder="Username">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" required="" placeholder="Password" name="password">
                    </div>
                </div>

                
                
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">登录</button>
                    </div>
                </div>

                

                {{csrf_field()}}
            </form> 
            
            </div>   
            </div>                              
                
            
        </div>
        
        

        
    	

        <!-- jQuery  -->
        <script src="{{asset('purple/assets/js/jquery.min.js')}}"></script>
        <script src="{{asset('purple/assets/js/bootstrap.min.js')}}"></script>
      
	
	</body>
</html>