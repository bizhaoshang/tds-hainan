@extends('admin.layouts.app')
@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">内容管理</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('zadmin/')}}">系统</a></li>

                <li class="active">内容列表</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">

                <div class="row">
                    <div class="col-sm-4">
                        <form role="form" href="{{ route('department.index') }}">
                            <div class="form-group contact-search col-sm-8 m-b-30">
                                <input type="text" id="search" class="form-control"  name="name" value="{{ Request()->name ?? '' }}" placeholder="输入标题搜索">

                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                            </div> <!-- form-group -->
                        </form>
                    </div>



                    <div class="col-sm-7">

                       
                            <a href="{{  url('zadmin/content/create') }}" class="btn btn-primary btn-md waves-effect waves-light m-b-30"
                            ><i class="md md-add"></i>添加</a>
                       


                    </div>
                </div>

              

                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif

                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>标题</th>
                            <th>分类</th>
                            <th>图片</th>
                            <th>内容</th>
                            <th>开关</th>
                            <th style="width: 200px;">操作</th>
                        </tr>
                        </thead>

                        <tbody>
                       
                            @foreach($list as $k =>$v)
                            <tr>
                                <td>{{$v->id}}</td>
                                <td>
                                    {{$v->title}}
                                </td>
                                <td>{{object_get($v,'cate.name')}}</td>
                                <td>
                                    <img src="{{$v->pic}}" width="45" />
                                </td>
                                
                                <td>
                                   {!!   str_limit($v->content,50) !!}
                                </td>
                                <td>
                <span class="btn btn-sm btn-{{$v->status==1?'success':'danger'}}">{{$v->status==1?'开启':'关闭'}}</span>  
                            
                                </td>

                                        <td>
    <a href="{{ url('zadmin/content/'.$v->id.'/edit') }}" class="btn btn-sm btn-info" >编辑</a>
    <a href="{{url('zadmin/content',$v->id)}}" class="btn btn-sm btn-danger" data-method="delete" 
  data-token="{{csrf_token()}}" data-confirm="确定删除吗?">删除</a>
                                           
                                        </td>
                               

                            </tr>
                            @endforeach
                        







                        </tbody>
                    </table>


                </div>
 {{ $list->links() }}
                </div>

        </div> <!-- end col -->


    </div>

@endsection



@section('modal')
    <!-- Modal -->
    <div id="custom-modal" class="modal-demo">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only">Close</span>
        </button>
        <h4 class="custom-modal-title">添加配件</h4>
        <div class="custom-modal-text text-left">
            <form class="form-horizontal" role="form" action="{{url('zadmin/goods')}}" method="post" enctype="multipart/form-data">

                <div class="form-group">
                    <label class="col-md-2 control-label">地点名称</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="name" value="" required="">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-2 control-label">备注</label>
                    <div class="col-md-10">
                        <textarea class="form-control" rows="5" name="remark"></textarea>
                    </div>
                </div>

                <div class="form-group text-center">

                    <button type="submit" class="btn btn-info waves-effect waves-light">保存</button>
                </div>


                {{csrf_field()}}
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $('#checkAll').click(function () {
            $('input:checkbox').prop('checked', this.checked);
        });


        $('.bathDel').on('click',function(){
            var ids = $('.check_class:checked').map(function() {
                return this.value;
            }).get();
            if(ids==''){
                alert('请选择要删除的数据');return;
            }
            var url="{{ route('body.deleteAll') }}";
            var data = {
                'ids':ids,
                '_method':'DELETE',
                '_token' :'{{ csrf_token() }}'
            };
            // data.ids = ids;
            {{--data._token = "{{csrf_token()}}";--}}
            $.post(url,data,function(data){
                // if(data,code == 200) {
                window.location.reload();
                // }
            },'json');
            // console.info(ids);
        });

        function display(id) {
            if($('.son_'+id).attr('hidden') == 'hidden'){
                $('.son_'+id).removeAttr('hidden');
            } else {
                $('.son_'+id).attr('hidden',true);
            }
        }

        function delete_body(id) {
            if(!confirm('您确定要删除'+id)){
                return false;
            }

            $.post('{{ route('body.delete') }}',{'id':id,'_token':'{{ csrf_token() }}','_method':'DELETE'},function (data) {
                if(data.code == 200){
                    window.location.href = '{{ route('body.index') }}';
                }
            },'json');

            return false;
        }


        function content_status(id,status) {
            if(id == '' && status == '') {
                return false;
            }
            if(status == 1) {
                status = 0;
            } else {
                status = 1;
            }
            

        }
    </script>
@endsection
