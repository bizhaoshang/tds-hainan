@extends('admin.layouts.app')
@include('vendor.ueditor.assets')
@section('css')
    <link href="{{asset('admin/plugins/select2/css/select2.css')}}" rel="stylesheet">
@endsection
@section('content')


    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">公告管理</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('zadmin/')}}">系统</a></li>
                <li><a href="{{ route('content.index') }}">公告列表</a></li>
                <li class="active">修改公告</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h2 class="m-t-0 header-title">修改内容</h2>
                <hr/>
                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" role="form" action="{{url('zadmin/content/'.$content->id)}}" method="post" enctype="multipart/form-data">

 {{ method_field('PUT') }}
                <div class="form-group">
                  <label class="col-md-2 control-label">标题</label>
                  <div class="col-md-10">
                      <input type="text" class="form-control" name="title" value="{{$content->title}}">
                  </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">图片</label>
                    <div class="col-md-8">
                        <input type="file" class="form-control" name="pic">
                    </div>
                    <div class="col-md-2">
                        @if($content->pic)
                        <img src="{{$content->pic}}" width="45" />
                        @endif
                    </div>
                </div>

               <div class="form-group">
                  <label class="col-md-2 control-label">分类</label>
                  <div class="col-md-10">
                    <select  class="form-control" name="cate_id" required="" >
                     <option value="">请选择</option>
                    @foreach($cates as $cate)
                    <option value="{{$cate->id}}" {{$cate->id==$content->cate_id?'selected':''}}>{{$cate->name}}</option>
                    @endforeach
                    </select>
                  </div>
              </div>
<div class="form-group">
                  <label class="col-md-2 control-label">内容</label>
                  <div class="col-md-10">

                            <script type="text/javascript">
                                var ue = UE.getEditor('container');
                                ue.ready(function() {
                                    ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
                                });
                            </script>
                            <!-- 编辑器容器 -->
                            <script id="container" name="content" type="text/plain">{!!  $content->content  !!}</script>
</div>
</div>

            <div class="form-group">
                  <label class="col-md-2 control-label">状态</label>
                  <div class="col-md-10">
                      <input type="radio" name="status" value="1" {{$content->status==1?'checked':''}}>开启
                      <input type="radio" name="status" value="0" {{$content->status==0?'checked':''}}>
                      关闭
                  </div>
              </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">相关科室</label>
                    <div class="col-md-10">
                        <select multiple=""  class="form-control select2" name="department_ids[]" id="department_ids" >
                            <option value="">请选择</option>
                            @foreach($departments as $d)
                            <option value="{{$d->id}}" {{in_array($d->id,$department_ids)?'selected':''}}>{{$d->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>  



                            {{csrf_field()}}
                            <div class="form-group text-center col-md-12">
                                <button type="submit" class="btn btn-info waves-effect waves-light">保存</button>
                            </div>
                        </form>

                    </div>

                </div>
            </div>

        </div> <!-- end col -->


    </div>
@endsection






@section('modal')
    
@endsection







@section('js')

    <script type="text/javascript">


    </script>
@endsection
@section('js')
<script type="text/javascript" src="{{asset('admin/plugins/select2/js/select2.min.js')}}"></script>
    <script type="text/javascript">

            $('#department_ids').select2(); 
    </script>
@endsection

