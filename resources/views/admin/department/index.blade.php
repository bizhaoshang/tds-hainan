@extends('admin.layouts.app')
@section('content')
    <style>
        #department::-webkit-scrollbar {
            display: none;
        }
    </style>
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">科室管理</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('zadmin/')}}">系统</a></li>

                <li class="active">科室列表</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-4">
                        <form role="form" href="{{ route('department.index') }}">
                            <div class="form-group contact-search col-sm-8 m-b-30">
                                <input type="text" id="search" class="form-control"  name="name" value="{{ Request()->name ?? '' }}" placeholder="输入科室名搜索">

                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                            </div> <!-- form-group -->
                        </form>
                    </div>



                    <div class="col-sm-7">

                       
                            <a href="{{  url('zadmin/department/create') }}" class="btn btn-primary btn-md waves-effect waves-light m-b-30"
                            ><i class="md md-add"></i>添加</a>
                       


                    </div>
                </div>

                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif

                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>科室名称</th>
                            <th>图片</th>
                            <th>标签</th>
                            <th>对应疾病</th>
                            <th>状态</th>
                            <th>操作</th>
                        </tr>
                        </thead>

                        @if($dep)
                        <tbody>
                            @foreach($dep as $v)
                                <tr>
                                    <td>{{$v->id}}</td>

                                    <td>
                                        {{$v->name}}
                                    </td>
                                    <td>
                                        <img src="{{$v->pic}}" width="50"/>
                                    </td>

                                    <td id="department">
                                        @foreach($v->tags as $vv)
                                            <span style="float:left;border: 1px solid;border-radius: 10px;padding: 3px;">{{ $vv->name }}</span>
                                        @endforeach
                                    </td>

                                    <td>
                                        <a href="{{url('zadmin/disease/symptom/'.$v->id)}}">{{$v->name}}({{$v->diseases->count()}})</a>
                                    </td>
                                    <td>
                                        <span class="btn btn-sm btn-{{$v->status==1?'success':'danger'}}">
                                            {{$v->status==1?'开启':'关闭'}}
                                        </span>
                                    </td>
                                    <td>
                                        <a href="{{ url('zadmin/department/'.$v->id.'/edit') }}" ><i class="md md-edit"></i>编辑</a>
            <a href="{{url('zadmin/department/delete?id='.$v->id)}}" data-method="delete" 
  data-token="{{csrf_token()}}" data-confirm="确定删除吗?"><i class="md md-close"></i>删除</a> 
                                    </td>
                                </tr>

                            @endforeach
                            {!!  $dep->appends(Request::all())->render()  !!}
                        @else

                            暂无数据~
                        @endif


                        </tbody>
                    </table>


                </div>


            </div>

        </div> <!-- end col -->


    </div>
@endsection






@section('modal')
    
@endsection

@section('js')
    <script type="text/javascript">
        $('#checkAll').click(function () {
            $('input:checkbox').prop('checked', this.checked);
        });


        $('.bathDel').on('click',function(){
            var ids = $('.check_class:checked').map(function() {
                return this.value;
            }).get();
            if(ids==''){
                alert('请选择要删除的数据');return;
            }
            var url="{{url('zadmin/disease/bath-del')}}";
            var data = {};
            data.ids = ids;
            data._token = "{{csrf_token()}}";
            console.log(url);
            $.post(url,data,function(rs){

                if(rs.status==true) {
                    window.location.reload();
                }
            });
            console.info(ids);
        });

        

    </script>
@endsection









