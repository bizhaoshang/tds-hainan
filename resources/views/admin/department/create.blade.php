@extends('admin.layouts.app')
@include('vendor.ueditor.assets')
@section('content')
    <style>
        .form-group::-webkit-scrollbar {
            display: none;
        }

    </style>
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">科室管理</h4>
            <ol class="breadcrumb">
                <li><a href="{{url('zadmin/')}}">系统</a></li>
                <li><a href="{{route('department.index')}}">科室列表</a></li>
                <li class="active">新建科室</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h2 class="m-t-0 header-title">新建疾病</h2>
                <hr/>
                @if(session('rs'))
                    <div class="alert alert-{{session('rs')['status']}}">
                        {{ session('rs')['msg'] }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-9">
                        <form class="form-horizontal" role="form" action="{{ url('zadmin/department')}}" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-md-3 control-label">科室名称</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="name" required="" value="{{ old('name') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">图片</label>
                                <div class="col-md-9">
                                    <input type="file" class="form-control" name="pic" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">标签</label>
                                <div class="col-md-9" style="">
                                    @foreach($tag as $v)
                                        <label><input type="checkbox" name="tags[]" style="flaot:left;margin:4px" value="{{$v->id}}">{{ $v->name }}</label>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">地址</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="href" required="" value="{{ old('name') }}">

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">内容</label>
                                <div class="col-md-9">
                                    <script type="text/javascript">
                                        var ue = UE.getEditor('container');
                                        ue.ready(function() {
                                            ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
                                        });
                                    </script>

                                    <!-- 编辑器容器 -->
                                    <script id="container" name="contents" type="text/plain"></script>
                                </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-3 control-label">状态</label>
                            <div class="col-md-9">
                                <input type="radio"  name="status"  value="1" checked="" >开放
                                <input type="radio"  name="status"  value="0" >关闭
                            </div>
                        </div>

                            <div class="form-group text-center col-md-12">

                                <button type="submit" class="btn btn-info waves-effect waves-light">保存</button>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>

                </div>

            </div>

        </div> <!-- end col -->


    </div>
@endsection

@section('js')

    <script type="text/javascript">


       
    </script>
@endsection

