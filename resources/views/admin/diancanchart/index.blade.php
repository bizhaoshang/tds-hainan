﻿<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>index</title>
<script type="text/javascript" src="{{asset('chart/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('chart/js/echarts.min.js')}}"></script>

<script type="text/javascript" src="{{asset('chart/js/jquery.liMarquee.js')}}"></script>
<script type="text/javascript" src="{{asset('chart/js/jquery.cxselect.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('chart/css/comon0.css')}}">
</head>
<body>
<div style="background:#000d4a url({{asset('chart/images/bg.jpg')}}) center top;">
  <div class="loading">
    <div class="loadbox"> <img src="{{asset('chart/images/loading.gif')}}"> 页面加载中... </div>
  </div>
  <div class="back"></div>
  <div class="head">
    <h1>大数据可视化展示平台通用模板</h1>
    <div class="weather"><span id="showTime"></span></div>
	    </div>
    <script>
var t = null;
    t = setTimeout(time,1000);//開始运行
    function time()
    {
       clearTimeout(t);//清除定时器
       dt = new Date();
		var y=dt.getFullYear();
		var mt=dt.getMonth()+1;
		var day=dt.getDate();
       var h=dt.getHours();//获取时
       var m=dt.getMinutes();//获取分
       var s=dt.getSeconds();//获取秒
       document.getElementById("showTime").innerHTML = y+"年"+mt+"月"+day+"日"+h+"时"+m+"分"+s+"秒";
       t = setTimeout(time,1000); //设定定时器，循环运行     
    } 
</script> 
  <div class="mainbox">
    <ul class="clearfix">
      <li>
        <div class="boxall" style="height:545px;">
    
          <div class="navboxall" >
			<div class="sycm">
          <ul class="clearfix">
            <li>
              <h2>22864</h2>
              <span>总金额</span></li>
            <li>
              <h2>1572</h2>
              <span>数量</span></li>

          </ul>
          
        </div>
			  
			  <ul class="jindu clearfix">
				  <div>1000</div>
				  <div>2000</div>
			  <li id="zb1"></li>
			  <li id="zb2"></li>
			  <li id="zb3"></li>
			  <li id="zb4"></li>
			  </ul>
			</div>
        </div>
      </li>
      <li>
        <div class="boxall" style="height:545px">
          <div class="alltitle">订单</div>
          <div class="navboxall" id="echart4"></div>
        </div>
      </li>
      <li>
        <div class="boxall" style="height:260px">
          <div class="alltitle">部门</div>
          <div class="navboxall" id="echart1"> </div>
        </div>
        <div class="boxall" style="height:270px">
          <div class="alltitle">商户</div>
          <div class="navboxall"  id="echart2"> </div>
        </div>
      </li>
    </ul>
    <ul class="clearfix">
      <li>
        <div class="boxall" style="height:390px;">
          <div class="alltitle">标题样式</div>
          <div class="navboxall" >
            <div class="wraptit"> <span>订单号</span><span>订单金额</span><span>计划时间</span><span>当前状态</span> </div>
            <div class="wrap">
              <ul>
                <li>
                  <p><span>100021415</span><span>199</span><span>手机</span><span>18小时</span></p>
                </li>
                <li>
                  <p><span>100021415</span><span>199</span><span>手机</span><span>18小时</span></p>
                </li>
                <li>
                  <p><span>100021415</span><span>199</span><span>手机</span><span>18小时</span></p>
                </li>
                <li>
                  <p><span>100021415</span><span>199</span><span>手机</span><span>18小时</span></p>
                </li>
                <li>
                  <p><span>100021415</span><span>199</span><span>手机</span><span>18小时</span></p>
                </li>
                <li>
                  <p><span>100021415</span><span>199</span><span>手机</span><span>18小时</span></p>
                </li>
                <li>
                  <p><span>100021415</span><span>199</span><span>手机</span><span>18小时</span></p>
                </li>
                <li>
                  <p><span>100021415</span><span>199</span><span>手机</span><span>18小时</span></p>
                </li>

				</ul>
            </div>
          </div>
        </div>
      </li>
      <li style="width:38%">
        <div class="boxall" style="height:390px">
          <div class="alltitle">部门统计</div>
          <div class="navboxall" id="echart3"></div>
        </div>
      </li>
      <li style="width:38%">
        <div class="boxall" style="height:390px">
          <div class="alltitle">商户统计</div>
          <div class="navboxall" id="echart5"></div>
        </div>
      </li>
    </ul>
  </div>
</div>
	<!---以下代码和演示无关，请自行删除-->

	

<script>
$(function(){
	$('.wrap').liMarquee({
		direction: 'up',//身上滚动 
		//runshort: false,//内容不足时不滚动
		scrollamount: 20//速度
	});
});

 $(window).load(function(){$(".loading").fadeOut()})  
$(function () {
    echarts_1();
  echarts_2();
  echarts_3();
  echarts_4(); // 订单柱形图
  echarts_5();
  zb1();
  zb2();
  zb3();
  zb4();
function echarts_1() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('echart1'));
        option = {
                    tooltip : {
                        trigger: 'item',
                        formatter: "{b} : {c} ({d}%)"
                    },
                    legend: {
                        right:10,
                        top:30,
                        height:140,
                        itemWidth:10,
                        itemHeight:10,
                        itemGap:10,
                        textStyle:{
                            color: 'rgba(255,255,255,.6)',
                            fontSize:12
                        },
                        orient:'vertical',
                        data:['图例1','图例2','图例3','图例4']
                    },
                   calculable : true,
                    series : [
                        {
                            name:' ',
              color: ['#62c98d', '#2f89cf', '#4cb9cf', '#53b666', '#62c98d', '#205acf', '#c9c862', '#c98b62', '#c962b9', '#7562c9','#c96262'],  
                            type:'pie',
                            radius : [30, 70],
                            center : ['35%', '50%'],
                            roseType : 'radius',
                            label: {
                                normal: {
                                    show: true
                                },
                                emphasis: {
                                    show: true
                                }
                            },

                            lableLine: {
                                normal: {
                                    show: false
                                },
                                emphasis: {
                                    show: true
                                }
                            },

                            data:[
                                {value:10, name:'图例1'},
                                {value:15, name:'图例2'},
                                {value:25, name:'图例3'},
                                {value:30, name:'图例4'}
                            ]
                        },
                    ]
                };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        window.addEventListener("resize",function(){
            myChart.resize();
        });
    }
function echarts_2() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('echart2'));

        option = {
                    tooltip : {
                        trigger: 'item',
                        formatter: "{b} : {c} ({d}%)"
                    },
                    legend: {
                        right:10,
                        top:30,
                        height:140,
                        itemWidth:10,
                        itemHeight:10,
                        itemGap:10,
                        textStyle:{
                            color: 'rgba(255,255,255,.6)',
                            fontSize:12
                        },
                        orient:'vertical',
                        data:['图例1','图例2','图例3','图例4']
                    },
                   calculable : true,
                    series : [
                        {
                            name:' ',
              color: ['#62c98d', '#205acf', '#c9c862', '#c98b62', '#c962b9', '#7562c9','#c96262'],  
                            type:'pie',
                            radius : [30, 70],
                            center : ['35%', '50%'],
                            roseType : 'radius',
                            label: {
                                normal: {
                                    show: true
                                },
                                emphasis: {
                                    show: true
                                }
                            },

                            lableLine: {
                                normal: {
                                    show: true
                                },
                                emphasis: {
                                    show: true
                                }
                            },

                            data:[
                                {value:50, name:'图例1'},
                                {value:45, name:'图例2'},
                                {value:35, name:'图例3'},
                                {value:30, name:'图例4'}
                            ]
                        },
                    ]
                };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        window.addEventListener("resize",function(){
            myChart.resize();
        });
    }
function echarts_3() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('echart3'));

       option = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
            lineStyle: {
                color: '#dddc6b'
            }
        }
    },
    grid: {
        left: '10',
    top: '20',
        right: '30',
        bottom: '10',
        containLabel: true
    },

    xAxis: [{
        type: 'category',
        boundaryGap: false,
axisLabel:  {
                textStyle: {
          color: "rgba(255,255,255,.6)",
          fontSize:16,
                },
            },
        axisLine: {
      lineStyle: { 
        color: 'rgba(255,255,255,.1)'
      }

        },

   data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月']

    }, {

        axisPointer: {show: false},
        axisLine: {  show: false},
        position: 'bottom',
        offset: 20,

       

    }],

    yAxis: [{
        type: 'value',
        axisTick: {show: false},
        axisLine: {
            lineStyle: {
                color: 'rgba(255,255,255,.1)'
            }
        },
       axisLabel:  {
                textStyle: {
          color: "rgba(255,255,255,.6)",
          fontSize:16,
                },
            },

        splitLine: {
            lineStyle: {
                 color: 'rgba(255,255,255,.1)'
            }
        }
    }],
    series: [
    {
        name: '结算率',
        type: 'line',
        smooth: true,
        symbol: 'circle',
        symbolSize: 5,
        showSymbol: false,
        lineStyle: {
      
            normal: {
        color: '#dddc6b',
                width: 4
            }
        },
        areaStyle: {
            normal: {
                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                    offset: 0,
                    color: 'rgba(221, 220, 107, 0.4)'
                }, {
                    offset: 0.8,
                    color: 'rgba(221, 220, 107, 0.1)'
                }], false),
                shadowColor: 'rgba(0, 0, 0, 0.1)',
            }
        },
      itemStyle: {
      normal: {
        color: '#dddc6b',
        borderColor: 'rgba(221, 220, 107, .1)',
        borderWidth: 12
      }
    },
        data: [3, 4, 3, 4, 3, 4, 3, 6, 2, 4, 2, 4]

    }, 

     ]

};

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        window.addEventListener("resize",function(){
            myChart.resize();
        });
    }
function echarts_4() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('echart4'));
option = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    legend: {
        data: ['订单量', '订单金额'],
        align: 'right',
        right: '40%',
    top:'0%',
        textStyle: {
            color: "#fff",
        fontSize: '16',

        },
        itemWidth: 16,
        itemHeight: 16,
        itemGap: 35
    },
    grid: {
        left: '0%',
    top:'40px',
        right: '0%',
        bottom: '2%',
       containLabel: true
    },
    xAxis: [{
        type: 'category',
          data: ['1月', '2月', '3月', '4月', '5月', '6月'],
        axisLine: {
            show: true,
         lineStyle: {
                color: "rgba(255,255,255,.1)",
                width: 1,
                type: "solid"
            },
        },
    
        axisTick: {
            show: false,
        },
    axisLabel:  {
                interval: 0,
               // rotate:50,
                show: true,
                splitNumber: 15,
                textStyle: {
          color: "rgba(255,255,255,.6)",
                    fontSize: '16',
                },
            },
    }],
    yAxis: [{
        type: 'value',
        axisLabel: {
           //formatter: '{value} %'
      show:true,
       textStyle: {
          color: "rgba(255,255,255,.6)",
                    fontSize: '16',
                },
        },
        axisTick: {
            show: false,
        },
        axisLine: {
            show: true,
            lineStyle: {
                color: "rgba(255,255,255,.1 )",
                width: 1,
                type: "solid"
            },
        },
        splitLine: {
            lineStyle: {
               color: "rgba(255,255,255,.1)",
            }
        }
    }],
    series: [{
        name: '订单量',
        type: 'bar',
        data: [6, 4, 6, 7, 4, 10],
        barWidth:'15', //柱子宽度
       // barGap: 1, //柱子之间间距
        itemStyle: {
            normal: {
                color:'#2f89cf',
                opacity: 1,
        barBorderRadius: 5,
            }
        }
    }, {
        name: '订单金额',
        type: 'bar',
    data: [ 5, 6, 5, 6, 3, 9],
    barWidth:'15',
       // barGap: 1,
        itemStyle: {
            normal: {
                color:'#62c98d',
                opacity: 1,
        barBorderRadius: 5,
            }
        }
    },
  ]
};
       

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        window.addEventListener("resize",function(){
            myChart.resize();
        });
    }
function echarts_5() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('echart5'));
option = {
  //  backgroundColor: '#00265f',
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    legend: {
        data: ['2017年', '2018年'],
        align: 'right',
        right: '40%',
    top:'0%',
        textStyle: {
            color: "#fff",
        fontSize: '16',

        },
 
        itemGap: 35
    },
    grid: {
        left: '0%',
    top:'40px',
        right: '0%',
        bottom: '2%',
       containLabel: true
    },
    xAxis: [{
        type: 'category',
          data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
        axisLine: {
            show: true,
         lineStyle: {
                color: "rgba(255,255,255,.1)",
                width: 1,
                type: "solid"
            },
        },
        axisTick: {
            show: false,
        },
    axisLabel:  {
                interval: 0,
               // rotate:50,
                show: true,
                splitNumber: 15,
                textStyle: {
          color: "rgba(255,255,255,.6)",
                    fontSize: '16',
                },
            },
    }],
    yAxis: [{
        type: 'value',
        axisLabel: {
           //formatter: '{value} %'
      show:true,
       textStyle: {
          color: "rgba(255,255,255,.6)",
                    fontSize: '16',
                },
        },
        axisTick: {
            show: false,
        },
        axisLine: {
            show: true,
            lineStyle: {
                color: "rgba(255,255,255,.1 )",
                width: 1,
                type: "solid"
            },
        },
        splitLine: {
            lineStyle: {
               color: "rgba(255,255,255,.1)",
            }
        }
    }],
    series: [{
        name: '2017年',
        type: 'line',
    
        data: [2, 6, 3, 8, 5, 8, 10, 13, 8, 5, 6, 9],
       
        itemStyle: {
            normal: {
                color:'#2f89cf',
                opacity: 1,
        
        barBorderRadius: 5,
            }
        }
    }, {
        name: '2018年',
        type: 'line',
        data: [5, 2, 6, 4, 5, 12, 5, 17, 9, 2, 6, 3],
    barWidth:'15',
       // barGap: 1,
        itemStyle: {
            normal: {
                color:'#62c98d',
                opacity: 1,
        barBorderRadius: 5,
            }
        }
    },
  ]
};
       

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        window.addEventListener("resize",function(){
            myChart.resize();
        });
    }

function zb1() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('zb1'));
    var v1=60//结算数
    var v2=40//未结算数
    var v3=v1+v2//总订单数
option = {  
    series: [{
        type: 'pie',
        radius: ['60%', '70%'],
        color:'#49bcf7',
        label: {
            normal: {
                position: 'center'
            }
        },
        data: [{
            value: v1,
            name: '数量结算率',
            label: {
                normal: {
                    formatter:Math.round( v1/v3*100)+ '%',
                    textStyle: {
                        fontSize: 30,
            color:'#fff',
                    }
                }
            }
        }, 
         {
            value: v2,
            label: {
                normal: {
                 formatter : function (params){
                return '数量结算率'
            },
                    textStyle: {
                        color: '#aaa',
                        fontSize: 16
                    }
                }
            },
            itemStyle: {
                normal: {
                    color: 'rgba(255,255,255,.2)'
                },
                emphasis: {
                    color: '#fff'
                }
            },
        }]
    }]
};
        myChart.setOption(option);
        window.addEventListener("resize",function(){
            myChart.resize();
        });
    }
function zb2() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('zb2'));
    var v1=80//结算数
    var v2=20//未结算数
    var v3=v1+v2//总订单数
      option = {
  
//animation: false,
    series: [{  
        type: 'pie',
       radius: ['60%', '70%'],
        color:'#49bcf7',
        label: {
            normal: {
                position: 'center'
            }
        },
        data: [{
            value: v1,
            name: '数量结算率',
            label: {
                normal: {
                    formatter:Math.round( v1/v3*100)+ '%',
                    textStyle: {
                        fontSize: 24,
            color:'#fff',
                    }
                }
            }
        }, {
            value: v2,
            label: {
                normal: {
                 formatter : function (params){
                return '数量结算率'
            },
                    textStyle: {
                        color: '#aaa',
                        fontSize: 16
                    }
                }
            },
            itemStyle: {
                normal: {
                    color: 'rgba(255,255,255,.2)'
                },
                emphasis: {
                    color: '#fff'
                }
            },
        }]
    }]
};
        myChart.setOption(option);
        window.addEventListener("resize",function(){
            myChart.resize();
        });
    }
function zb3() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('zb3'));
    var v1=30//结算金额
    var v2=70//未结算
    var v3=v1+v2
option = {  
    series: [{
    
        type: 'pie',
       radius: ['60%', '70%'],
        color:'#62c98d',
        label: {
            normal: {
                position: 'center'
            }
        },
        data: [{
            value: v1,
            name: '金额结算率',
            label: {
                normal: {
                    formatter:Math.round( v1/v3*100)+ '%',
                    textStyle: {
                        fontSize: 24,
            color:'#fff',
                    }
                }
            }
        }, {
            value: v2,
            label: {
                normal: {
                 formatter : function (params){
                return '金额结算率'
            },
                    textStyle: {
                        color: '#aaa',
                        fontSize: 16
                    }
                }
            },
            itemStyle: {
                normal: {
                    color: 'rgba(255,255,255,.2)'
                },
                emphasis: {
                    color: '#fff'
                }
            },
        }]
    }]
};
        myChart.setOption(option);
        window.addEventListener("resize",function(){
            myChart.resize();
        });
    }
function zb4() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('zb4'));
    var v1=90//结算金额
    var v2=10//未结算
    var v3=v1+v2
option = {  
    series: [{
    
        type: 'pie',
       radius: ['60%', '70%'],
        color:'#29d08a',
        label: {
            normal: {
                position: 'center'
            }
        },
        data: [{
            value: v1,
            name: '金额结算率',
            label: {
                normal: {
                    formatter:Math.round( v1/v3*100)+ '%',
                    textStyle: {
                        fontSize: 24,
            color:'#fff',
                    }
                }
            }
        }, {
            value: v2,
            label: {
                normal: {
                 formatter : function (params){
                return '金额结算率'
            },
                    textStyle: {
                        color: '#aaa',
                        fontSize: 16
                    }
                }
            },
            itemStyle: {
                normal: {
                    color: 'rgba(255,255,255,.2)'
                },
                emphasis: {
                    color: '#fff'
                }
            },
        }]
    }]
};
        myChart.setOption(option);
        window.addEventListener("resize",function(){
            myChart.resize();
        });
    }
})
</script> 

</body>
</html>
