<?php

use Illuminate\Http\Request;

Route::namespace('Wx')->middleware(['cors'])->group(function(){
	
	// get-openid
	Route::post('get-openid','HomeController@getOpenid');

	// get-wx-user
	Route::post('get-wx-user','HomeController@getWxUser');	

	// 获取token
	Route::post('get-token','HomeController@getToken');

	// 发送短信
	Route::post('send-sms','HomeController@pushSms');

	// 获取部门
	Route::post('get-depart','HomeController@getDepart');

	// 获取职位
	Route::post('get-position','HomeController@getPosition');

	// 故障类别
	Route::post('get-category','HomeController@getCategory');

	Route::post('upload',	'HomeController@upload');

	// 内部用户
	Route::prefix('user')->group(function(){
		Route::post('info',  'StaffController@info');
		Route::post('update','StaffController@update');
		Route::prefix('address')->group(function(){
			Route::post('index','StaffAddressController@index');
			Route::post('info', 'StaffAddressController@info');
			Route::post('add',  'StaffAddressController@store');
			Route::post('update',    'StaffAddressController@update');
			Route::post('del',       'StaffAddressController@delete');
			Route::post('setdefault','StaffAddressController@setdefault');
		});
	});

	// 点餐模块
	Route::prefix('diancan')->namespace('Diancan')->group(function(){
		Route::post('notice',      'HomeController@notice');
		Route::post('get-shops',   'ShopController@getShops');
		Route::post('shop-info',   'ShopController@info');
		Route::prefix('product')->group(function(){
			Route::post('list','ProductController@index');
			Route::post('cate','ProductController@cate');
		});
		Route::prefix('cart')->group(function(){
            Route::post('add',     'CartController@add');
            Route::post('list',    'CartController@index');
            Route::post('del',     'CartController@del');
            Route::post('increase','CartController@increase');
            Route::post('decrease','CartController@decrease');
            Route::post('del-all', 'CartController@delAll');
        });

        Route::prefix('order')->group(function(){
            Route::post('add',     'OrderController@add');
            Route::post('info',    'OrderController@info');
            Route::post('list',    'OrderController@index');
            Route::any('complete', 'OrderController@complete');
            Route::post('repeat',  'OrderController@repeat');
            Route::post('confirm-complete','OrderController@confirmComplete');
        });
	});

	// 工单模块
	Route::prefix('task')->namespace('Task')->group(function(){
		Route::post('notice','TaskController@notice');
		Route::post('index','TaskController@index');
		// 维修工列表
		Route::post('workers','WorkerController@index');
		Route::post('add','TaskController@save');
		Route::post('update','TaskController@update');
		Route::post('info','TaskController@info');
	});

	// 导诊
	Route::prefix('daozhen')->namespace('Dz')->group(function(){
		// 男人、女人。。。
		Route::post('tags',   'IndexController@tags');
		//首页科室显示
        Route::post('department',   'IndexController@department');

        // 科室关联的文章
         Route::post('department-content',   'IndexController@departmentContent'); 
		//症状
		Route::post('symptom','IndexController@symptom');

		Route::post('search', 'IndexController@search');

		// 身体部位图--症状
		Route::post('bodysearch','IndexController@bodySearch');
		
		Route::post('bodytab',  'IndexController@bodyTab');

		// 公告
		Route::post('notice',	'IndexController@notice');
		Route::post('symptom-search','IndexController@symptomSearch');		
		Route::post('searchdoctor','IndexController@searchDoctor');
		Route::post('symptom-fenxi','IndexController@symptomFenxi');
		Route::post('doctor','IndexController@doctor');
		// 疾病检索
		Route::post('disease-retrieve','IndexController@diseaseRetrieve');
		//根据科室ID获取医生
        Route::post('getdoctor','IndexController@getDoctorByDepartmentId');
        //获取问题列表
        Route::post('getquestion','IndexController@getQuestion');
        //获取问题列表下的回答列表
        Route::post('getanswer','IndexController@getAnswer');
        //获取问题解答详情
        Route::post('getanswercontent','IndexController@getAnswerContent');
        //获取科室信息
        Route::post('getoutpatient','IndexController@getOutpatient');
        Route::post('getoutpatientContent','IndexController@getoutpatientContent');
        Route::prefix('article')->group(function(){
        	Route::post('cate','ArticleController@articleCate');
        	Route::post('list','ArticleController@articleList');
        	Route::post('detail','ArticleController@articleDetail');
        });

        // 首页富文本单挑信息
         Route::post('home-info','HomeInfoController@show');

	});

});


